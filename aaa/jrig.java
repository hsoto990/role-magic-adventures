package rma;

import java.awt.Point;
import java.util.ArrayList;

public class jrig {
	int thisEventID = 1;

	Event hshe() {
		return DrawRect.mapa.events.get(thisEventID);
	}

	int[] AtackRange = { 2, 3 };
	double Maxlife = 100;
	double morale = 0;

	boolean walkable[][] = new boolean[17][17];

	void AIGeneric(int vthisEventID, int vMaxlife, int vmorale, int[] vAtackRange) {
		new Thread() {
			public void run() {
				thisEventID = vthisEventID;
				Maxlife = vMaxlife;
				morale = vmorale;
				AtackRange = vAtackRange;

//				while (hshe().durability > 0) {
					ArrayList<ArrayList<ArrayList<String>>> tags = new ArrayList<ArrayList<ArrayList<String>>>();

					for (int ix = 0; ix < 17; ix++) {
						tags.add(new ArrayList<ArrayList<String>>());
						int sumPosX = hshe().pos.x - 8 + ix;
						for (int iy = 0; iy < 17; iy++) {
							tags.get(ix).add(new ArrayList<String>());
							int sumPosY = hshe().pos.y - 8 + iy;
							try {
								if (DrawRect.mapa.grid[sumPosX][sumPosY].Tipo == "Suelo")
									walkable[ix][iy] = true;
								else
									walkable[ix][iy] = false;

								ArrayList<Integer> eicuad = DrawRect.mapa.eventsInCuad.get(sumPosX).get(sumPosY);
								for (int i = 0; i < eicuad.size(); i++) {
									tags.get(ix).get(iy).addAll(DrawRect.mapa.events.get(eicuad.get(i)).tags);
								}
							} catch (Exception e) {
								walkable[ix][iy] = false;
							}
						}
					}
					if (hshe().durability <= morale) {

					} else {

						if (iftaginGrid(tags, "Player")) {
							ArrayList<Point> finishes = new ArrayList<Point>();
							try {
								finishes.add(DrawRect.mapa.events.get(0).pos);
								new SinglePF().resolve(thisEventID, finishes, 300);
							} catch (Exception e) {
							}

						}
					}
//				}

			}
		}.start();
	}

	boolean iftaginGrid(ArrayList<ArrayList<ArrayList<String>>> tags, String tag) {
		boolean hig = false;
		for (int ix = 0; ix < 17; ix++) {
			for (int iy = 0; iy < 17; iy++) {
				if (tags.get(ix).get(iy).contains(tag)) {
					hig = true;
				}
			}
		}
		return hig;
	}
}
