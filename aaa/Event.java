package rma;

import java.awt.Point;
import java.util.ArrayList;

public class Event {
	int ID = 0;
	int inMap = 0;
	Point pos;
	double durability;
	double MaxDur;

	public String Chara;
	public String Pose;
	ArrayList<String> tags = new ArrayList<String>();
	boolean solid;
	boolean standing = true;
	boolean lookwhenmoved = false;

	Event(int vID, int vmap, int x, int y, double vdurability, String vChara, String vPose, boolean vphantom,
			boolean lwm) {
		ID = vID;
		inMap = vmap;
		pos = new Point(x, y);
		durability = vdurability;
		Chara = vChara;
		Pose = vPose;
		solid = vphantom;
		lookwhenmoved = lwm;
	}

	public void lookAtMock(int xmod, int ymod) {
		if (lookwhenmoved) {
			if (ymod == 1)
				Pose = ("S");
			else if (ymod == -1)
				Pose = ("N");
			else if (xmod == 1)
				Pose = ("E");
			else if (xmod == -1)
				Pose = ("W");
		}
	}

	void Move(int xmod, int ymod) {
		if (standing) {
			lookAtMock(xmod, ymod);
			standing = false;
			if (DrawRect.mapa.grid[pos.x + xmod][pos.y + ymod].Tipo == "Suelo") {
				teleport(pos.x + xmod, pos.y + ymod);
			}
			standing = true;
		}
	}

	public void teleport(int x, int y) {
		DrawRect.mapa.eventsInCuad.get(pos.x).get(pos.y).remove((Object) ID);
		pos = new Point(x, y);
		DrawRect.mapa.eventsInCuad.get(x).get(y).add(ID);
	}
}
