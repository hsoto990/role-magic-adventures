package rma;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Textures {
	static ArrayList<String> Bioma = new ArrayList<String>();
	static ArrayList<ArrayList<String>> Bloque = new ArrayList<ArrayList<String>>();
	static ArrayList<ArrayList<BufferedImage>> Textures = new ArrayList<ArrayList<BufferedImage>>();

	public static void init() {
		addBiomes("Nuclear");
		addBloque("Nuclear", "Border");
		addBloque("Nuclear", "Grass");

		addBiomes("DryLand");
		addBloque("DryLand", "Tree");
		addBloque("DryLand", "GrassMark");
		addBloque("DryLand", "Grass");

		addBiomes("Forest");
		addBloque("Forest", "Tree");
		addBloque("Forest", "Flower");
		addBloque("Forest", "Grass");

		addBiomes("GrassLand");
		addBloque("GrassLand", "Tree");
		addBloque("GrassLand", "GrassMark");
		addBloque("GrassLand", "Plant");
		addBloque("GrassLand", "Grass");

		addBiomes("Taiga");
		addBloque("Taiga", "Grass");
		addBloque("Taiga", "Tree");

		addBiomes("Tundra");
		addBloque("Tundra", "Snow");
		addBloque("Tundra", "Tree");

		addBiomes("Burned");
		addBloque("Burned", "Grass");
		addBloque("Burned", "Tree");

		addBiomes("Debris");
		addBloque("Debris", "Ground");

		addBiomes("Desert");
		addBloque("Desert", "Ground");

		addBiomes("General");
		addBloque("General", "Path");
		addBloque("General", "RockFloor");
		addBloque("General", "LogWall");

		addBiomes("Liquid");
		addBloque("Liquid", "Water");
		addBloque("Liquid", "Waste");
	}

	static void addBloque(String biomaaaa, String bloqueeee) {
		Bloque.get(Bioma.indexOf(biomaaaa)).add(bloqueeee);
		Textures.get(Bioma.indexOf(biomaaaa)).add(bi(biomaaaa+"/"+bloqueeee));
	}

	static void addBiomes(String biomaaaa) {
		Bioma.add(biomaaaa);
		Bloque.add(new ArrayList<String>());
		Textures.add(new ArrayList<BufferedImage>());
	}
	public static BufferedImage getText(String biomaaaa,String bloqueeee) {
		int bim = Bioma.indexOf(biomaaaa);
		try {
		return Textures.get(bim).get(Bloque.get(bim).indexOf(bloqueeee));
		}catch (Exception e) {
			System.out.println(biomaaaa +" "+ bloqueeee);
			return null;
		}
	}

	public static BufferedImage bi(String buff) {
		try {
			return ImageIO.read(new File("/home/ceibal/Escritorio/Resources/Maaaap/Graph/Tiles/" + buff + ".png"));
		} catch (IOException e) {
			System.out.println("Textura '" + buff + "' no encontrada");
			return null;
		}
	}
	
}
