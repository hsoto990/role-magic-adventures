package rma;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.*;

public class DrawRect extends JPanel {
	private static final long serialVersionUID = 1L;
	private static final int RECT_X = 0;
	private static final int RECT_Y = RECT_X;
	private static final int RECT_WIDTH = 530;
	private static final int RECT_HEIGHT = RECT_WIDTH;

	static Point playerPos = new Point(5, 5);

	static Graphics ham;

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Event narr = mapa.events.get(0);

		// draw the rectangle here
		g.drawRect(RECT_X, RECT_Y, RECT_WIDTH, RECT_HEIGHT);
		for (int ix = -1; ix <= 17; ix++) {
			int sumX = ix - 8 + narr.pos.x;
			if (sumX >= 0 & sumX < mapa.widht)
				for (int iy = -1; iy <= 17; iy++) {
					int sumY = iy - 8 + narr.pos.y;
					if (sumY >= 0 & sumY < mapa.height) {
						g.drawImage(mapa.grid[sumX][sumY].getTex(), ix * 32, iy * 32, 32, 32, null);
					} else {
						g.setColor(Color.black);
						g.fillRect(ix * 32, iy * 32, 32, 32);
					}
				}
			else {
				g.setColor(Color.black);
				for (int iy = -1; iy <= 17; iy++)
					g.fillRect(ix * 32, iy * 32, 32, 32);
			}
		}

		for (int ix = 0; ix <= 16; ix++) {
			int sumX = ix - 8 + narr.pos.x;
			if (sumX >= 0 & sumX < mapa.widht)
				for (int iy = 0; iy <= 16; iy++) {
					int sumY = iy - 8 + narr.pos.y;
					if (sumY >= 0 & sumY < mapa.height) {
						ArrayList<Integer> jaaaa = mapa.eventsInCuad.get(sumX).get(sumY);
						for (int ie = 0; ie < jaaaa.size(); ie++)
							try {
								Event hisa = mapa.events.get(jaaaa.get(ie));
								g.drawImage(
										ImageIO.read(new File("/home/ceibal/Escritorio/Resources/Maaaap/Graph/Charas/"
												+ hisa.Chara + "/" + hisa.Pose + ".png")),
										ix * 32, iy * 32, 32, 32, null);
							} catch (IOException e) {
								System.out.println("Textura '" + mapa.events.get(jaaaa.get(ie)).Chara + "/"
										+ mapa.events.get(jaaaa.get(ie)).Pose + "' no encontrada");
							}
					}
				}
		}
	}

	@Override
	public Dimension getPreferredSize() {
		// so that our GUI is big enough
		return new Dimension(RECT_WIDTH + 2 * RECT_X, RECT_HEIGHT + 2 * RECT_Y);
	}

	static Map mapa;
	public static JFrame frame = new JFrame("DrawRect");

	static Point PlayerMove = new Point();

	static void repaintLoop() {
		new Thread() {
			public void run() {
				while (true) {
					mapa.events.get(0).Move(PlayerMove.x, PlayerMove.y);
					PlayerMove.x = PlayerMove.y = 0;
					int[] jaaa = { 1, 1 };
					for (int i = 0; i < 10; i++)
						new jrig().AIGeneric(1+i, 100, 0, jaaa);

					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
					}
					frame.repaint();

				}
			}
		}.start();
	}

	// create the GUI explicitly on the Swing event thread
	private static void createAndShowGui() {
		Textures.init();
		try {
			mapa = new Map(0, ImageIO.read(new File("/home/ceibal/Escritorio/Resources/Maaaap/zombimapaa.png")));
			mapa.addEvent(playerPos.x, playerPos.y, 100, "Me", "S", false, true);
			mapa.events.get(0).tags.add("Player");
			for (int i = 0; i < 10; i++)
				mapa.addEvent(2, 2+i, 100, "Zombie", "S", false, true);

			repaintLoop();

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		DrawRect mainPanel = new DrawRect();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(mainPanel);
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
		frame.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				new Thread() {
					public void run() {
						if (("" + e.getKeyChar()).equalsIgnoreCase("W"))
							PlayerMove.y = -1;
						else if (("" + e.getKeyChar()).equalsIgnoreCase("A"))
							PlayerMove.x = -1;
						else if (("" + e.getKeyChar()).equalsIgnoreCase("S"))
							PlayerMove.y = +1;
						else if (("" + e.getKeyChar()).equalsIgnoreCase("D"))
							PlayerMove.x = +1;
					}
				}.start();
			}

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
			}
		});

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGui();
			}
		});
	}
}