package rma;

import java.awt.image.BufferedImage;

public class Tile {
	String Tipo = "Suelo";
	String Bioma;
	String Bloque;

	Tile(String tip, String vBioma, String vBloque) {
		Tipo = tip;
		Bioma=vBioma;
		Bloque=vBloque;
	}
	
	public BufferedImage getTex() {
		return Textures.getText(Bioma, Bloque);
	}
}
