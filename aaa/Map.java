package rma;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Map {
	int ID = 0;
	public int widht = 17;
	public int height = 17;
	Tile[][] grid = new Tile[widht][height];

	ArrayList<Event> events = new ArrayList<Event>();
	ArrayList<ArrayList<ArrayList<Integer>>> eventsInCuad = new ArrayList<ArrayList<ArrayList<Integer>>>();

	Map(int vID, BufferedImage icomand) {
		ID = vID;
		widht = icomand.getWidth();
		height = icomand.getHeight();
		grid = new Tile[widht][height];

		for (ix = 0; ix < widht; ix++) {
			eventsInCuad.add(new ArrayList<ArrayList<Integer>>());
			for (iy = 0; iy < height; iy++) {
				eventsInCuad.get(ix).add(new ArrayList<Integer>());
				tempBioma = "GrassLand";
				tempBloque = "Grass";
				tempTipo = "Suelo";
				icomandgetRGB = icomand.getRGB(ix, iy);
				thisrand = Math.random() * 100;
				BiomaNuclear();
				BiomaDryLand();
				BiomaForest();
				BiomaGrassLand();
				BiomaTaiga();
				BiomaTundra();
				BiomaBurned();
				BiomaDebris();
				BiomaDesert();
				BiomaRoad();
				BiomaRockFloor();
				BiomaLogWall();
				BiomaRiver();
				BiomaWasteRiver();
				BiomaWorldBorder();

				grid[ix][iy] = new Tile(tempTipo, tempBioma, tempBloque);
			}
		}
	}

	String tempBioma = "";
	String tempBloque = "";
	String tempTipo = "Suelo";

	double thisrand = 0;
	int ix = 0;
	int iy = 0;

	int icomandgetRGB = 0;

	void BiomaNuclear() {
		if (icomandgetRGB == new Color(175, 26, 25).getRGB()) {
			tempBioma = "Nuclear";
			tipobloque("Suelo", "Grass");
		}
	}

//	void addZombie(double prob, int x, int y) {
//		double monrand = Math.random() * 100;
//		if (monrand <= prob) {
//			DrawRect.mapa.addEvent(x, y, 100, "Zombie", "S", false, true);
//			int[] jaaa = { 1, 1 };
//			new jrig().AIGeneric(DrawRect.mapa.events.size() - 1, 100, 0, jaaa);
//		}
//	}

	void BiomaDryLand() {
		if (icomandgetRGB == new Color(153, 132, 69).getRGB()) {
			tempBioma = "DryLand";
			if (thisrand <= 1)
				tipobloque("Wall", "Tree");
			else if (thisrand <= 16)
				tipobloque("Suelo", "GrassMark");
			else
				tipobloque("Suelo", "Grass");
		}
	}

	void BiomaForest() {
		if (icomandgetRGB == new Color(44, 91, 54).getRGB()) {
			tempBioma = "Forest";
			if (thisrand <= 20)
				tipobloque("Wall", "Tree");
			else if (thisrand <= 30)
				tipobloque("Suelo", "Flower");
			else
				tipobloque("Suelo", "Grass");
		}
	}

	void BiomaGrassLand() {
		if (icomandgetRGB == new Color(67, 140, 65).getRGB()) {
			tempBioma = "GrassLand";
			if (thisrand <= 7)
				tipobloque("Wall", "Tree");
			else if (thisrand <= 15)
				tipobloque("Suelo", "GrassMark");
			else if (thisrand <= 22)
				tipobloque("Suelo", "Plant");
			else
				tipobloque("Suelo", "Grass");
		}
	}

	void BiomaTaiga() {
		if (icomandgetRGB == new Color(39, 115, 84).getRGB()) {
			tempBioma = "Taiga";
			if (thisrand <= 13)
				tipobloque("Wall", "Tree");
			else
				tipobloque("Suelo", "Grass");
		}
	}

	void BiomaTundra() {
		if (icomandgetRGB == new Color(255, 255, 255).getRGB()) {
			tempBioma = "Tundra";
			if (thisrand <= 7)
				tipobloque("Wall", "Tree");
			else
				tipobloque("Suelo", "Snow");
		}
	}

	void BiomaBurned() {
		if (icomandgetRGB == new Color(65, 65, 65).getRGB()) {
			tempBioma = "Burned";
			if (thisrand <= 14)
				tipobloque("Wall", "Tree");
			else
				tipobloque("Suelo", "Grass");

		}
	}

	void BiomaDebris() {
		if (icomandgetRGB == new Color(90, 90, 64).getRGB()) {
			tempBioma = "Debris";
			tipobloque("Suelo", "Ground");
		}
	}

	void BiomaDesert() {
		if (icomandgetRGB == new Color(250, 215, 150).getRGB()) {
			tempBioma = "Desert";
			tempBloque = "Ground";
		}
	}

	void BiomaRoad() {
		if (icomandgetRGB == new Color(255, 178, 127).getRGB()) {
			tempBioma = "General";
			tipobloque("Suelo", "Path");
		}
	}

	void BiomaRockFloor() {
		if (icomandgetRGB == new Color(160, 160, 160).getRGB()) {
			tempBioma = "General";
			tipobloque("Suelo", "RockFloor");
		}
	}

	void BiomaLogWall() {
		if (icomandgetRGB == new Color(127, 51, 0).getRGB()) {
			tempBioma = "General";
			tipobloque("Wall", "LogWall");
		}
	}

	void BiomaRiver() {
		if (icomandgetRGB == new Color(110, 140, 200).getRGB()) {
			tempBioma = "Liquid";
			tipobloque("Water", "Water");

		}
	}

	void BiomaWasteRiver() {
		if (icomandgetRGB == new Color(180, 255, 0).getRGB()) {
			tempBioma = "Liquid";
			tipobloque("Water", "Waste");
		}
	}

	void BiomaWorldBorder() {
		if (icomandgetRGB == new Color(127, 0, 0).getRGB()) {
			tempBioma = "Nuclear";
			tipobloque("Border", "Border");
		}
	}

	void tipobloque(String tip, String blo) {
		tempTipo = tip;
		tempBloque = blo;
	}

	public void addEvent(int x, int y, double vdurability, String vChara, String vPose, boolean vphantom, boolean lwm) {
		eventsInCuad.get(x).get(y).add(events.size());
		events.add(new Event(events.size(), ID, x, y, vdurability, vChara, vPose, vphantom, lwm));
	}

}
