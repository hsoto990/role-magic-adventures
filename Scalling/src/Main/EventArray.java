package Main;
import java.util.ArrayList;

public class EventArray {
	ArrayList<Event> coll = new ArrayList<Event>();
	void add(Event ev) {
		coll.add(ev);
	}
	Event get(int index) {
		return coll.get(index);
	}
	int last() {
		return coll.size()-1;
	}
}
