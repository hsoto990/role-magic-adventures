package Main;
import java.util.ArrayList;

public class MaterialList {

	static ArrayList<Material> materals = new ArrayList<Material>();

	static void addCustom(String vName, double vHardness, double vDensity, double vneededToBeTrusted,
			double vneededToBeCracked) {
		materals.add(new Material(vName, vHardness, vDensity, vneededToBeTrusted, vneededToBeCracked));
	}

	static void addMetalLike(String vName, double vHardness, double vDensMod, double vneededToBeTrusted) {
		addCustom(vName, vHardness, 10.000 * vDensMod, (360 - 15) * 30 * vneededToBeTrusted, 100000000);
	}

	static void addCrystalLike(String vName, double vHardness, double vDensMod, double vneededToBeTrusted,
			double vneededToBeCracked) {
		addCustom(vName, vHardness, 3.000 * vDensMod, (360 - 10) * 5000 * vneededToBeTrusted, 10 * vneededToBeCracked);
	}

	static void addLiquidLike(String vName, double vDensMod, double vneededToBeCracked) {
		addCustom(vName, 0, 1.000 * vDensMod, 0, 10 * vneededToBeCracked);
	}

	static void addOrganicLike(String vName, double vHardness, double vDensMod, double vneededToBeTrusted) {
		addCustom(vName, 0.5 * vHardness, 1.000 * vDensMod, (360 - 30) * 1 * vneededToBeTrusted, 100000000);
	}

	static void addBoneLike(String vName, double vHardness, double vDensMod, double vneededToBeTrusted,
			double vneededToBeCracked) {
		addCustom(vName, 2.0 * vHardness, 1.700 * vDensMod, (360 - 30) * 2 * vneededToBeTrusted,
				10 * vneededToBeCracked);
	}

	static Material get(String name) {
		Material daaa = materals.get(0);
		for (int i = 0; i < materals.size(); i++) {
			if (materals.get(i).Name.equals(name)) {
				daaa = materals.get(i);
				break;
			}
		}
		return daaa;
	}

	static void init() {
		addOrganicLike("Carne", 1, 1.1, 1);
		addLiquidLike("Grasa", 0.900, 1);
		materals.get(materals.size() - 1).BluntAbsorb = 1000;

		addOrganicLike("Testt", -17*2, 1.1, 1);
		materals.get(materals.size() - 1).BluntAbsorb = 100;
		
		addBoneLike("Colageno", 1.14, 0.7, 1, 5);
		addBoneLike("Hueso", 1.25, 1, 1, 1);
		addBoneLike("Madera", 1, 1, 1, 1);

		addLiquidLike("Agua", 1, 2);
		addCrystalLike("Hielo", 1.5, 0.3, 1, 0.2);
		addCrystalLike("Diamante", 10, 1.05, 1, 1);
		addCrystalLike("Corindon", 9, 1.5, 1, 1);
		addCrystalLike("Topacio", 8, 1, 1, 1);
		addCrystalLike("Cuarzo", 7, 1, 1, 1);
		addCrystalLike("Ortoclasa", 6, 1, 1, 1);
		addCrystalLike("Apatita", 5, 1, 1, 1);
		addCrystalLike("Fluorita", 4, 1, 1, 1);
		addCrystalLike("Calcita", 3, 1, 1, 1);
		addCrystalLike("Yeso", 2, 1, 1, 1);
		addCrystalLike("Talco", 1, 1, 1, 1);

		addCrystalLike("Vidrio", 6, 0.8, 1, 0.2);

		addMetalLike("Oro", 2.8, 1.929, 1);
		addMetalLike("Plata", 2.8, 1.05, 1);
		addMetalLike("Cobre", 3, 0.879, 1);
		addMetalLike("Platino", 4.2, 2.15, 1);
		addMetalLike("Hierro", 4.5, 0.787, 1);
		addMetalLike("Acero1", 5.75, 0.787, 1);
		addMetalLike("Acero2", 7, 0.787, 1);
	}

	static Material CopiMat(String copided) {
		Material tempTao = MaterialList.get(copided);
		Material retumat = new Material("temp", tempTao.Hardness, tempTao.Density, tempTao.neededToBeTrusted,
				tempTao.neededToBeCracked);
		retumat.BluntAbsorb = tempTao.BluntAbsorb;
		return retumat;

	}
}
