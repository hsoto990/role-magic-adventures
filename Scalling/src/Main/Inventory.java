package Main;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import Atributo.Atrib;
import Atributo.Atributos;

public class Inventory {
	ArrayList<Item> inven = new ArrayList<Item>();
	Atributos tempAtrib = new Atributos();
	Item RawPunch = new Item("-",Atributos.sett(new Atrib(0,1), new Atrib(0,1), new Atrib(0,1), new Atrib(0,1), new Atrib(0,1), new Atrib(1,1), new Atrib(0,1)),100,1 );

	
	Item Equiped = RawPunch;
	int selItem = 0;

	void add(Item mikusi) {
		inven.add(mikusi);
	}

	void show(Graphics g) {
		g.setColor(new Color(180, 210, 210));
		g.fillRect(0, 0, 530, 530);
		g.setColor(new Color(200, 255, 255));
		g.fillRect(50 - 8, 180 - 14, 400, 18);
		g.setColor(Color.black);
		g.drawString(Equiped.name, 50, 30);
		for (int i = selItem - 5; i < selItem + 5; i++) {
			if (i >= 0 & i < inven.size())
				g.drawString(inven.get(i).name, 50, 180 + 20 * (i - selItem));
		}
	}

	void equip() {
		if (inven.size() > 0) {
			Item tempSaaaa = Equiped;
			Equiped = inven.get(selItem);
			inven.remove(selItem);
			if (!tempSaaaa.name.equals("-"))
				inven.add(tempSaaaa);
		}
	}

	void unequip() {
		if (!Equiped.name.equals("-")) {
			inven.add(Equiped);
			Equiped = RawPunch;
		}
	}

}
