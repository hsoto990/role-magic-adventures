package Main;

import Atributo.Atributos;

public class EventPrefabs {
	public static Event tree(int x, int y) {
		Event preb = new Event(x, y, "Oak");
		preb.mode = "TreeGrowing";
		preb.EV.add("Maturity", 0);
		preb.EV.add("State", 1);
		preb.setImage("bTree/5");
		Atributos tempAtrib = new Atributos();
		tempAtrib.Aplastante.Defensa=4;
		preb.itemOfEvent = new Item("Chopped Tree", tempAtrib, 100, 80);
		return preb;
	}

	public static Event rock(int x, int y) {
		Event preb = new Event(x, y, "Rock");
		preb.setImage("Rocka");
		Atributos tempAtrib = new Atributos();
		preb.itemOfEvent = new Item("Giant Rock", tempAtrib, 100, 80);
		return preb;
	}

	public static Event human(int x, int y) {
		Event preb = new Event(x, y, "Human");
		preb.WalkOn.add("Ground");
		preb.WalkOn.add("Red Carpet");
		preb.WalkOn.add("Water");
		preb.speed = 4;
		Atributos tempAtrib = new Atributos();
		preb.itemOfEvent = new Item("Human Corpse",tempAtrib,100,80 );		return preb;
	}

	public static Event fish(int x, int y) {
		Event preb = new Event(x, y, "Fish");
		preb.WalkOn.add("Water");
		preb.mode = "Roam";
		Atributos tempAtrib = new Atributos();
		preb.itemOfEvent = new Item("Dead Fish",tempAtrib,100,80 );
		return preb;
	}
}
