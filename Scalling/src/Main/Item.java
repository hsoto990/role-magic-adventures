package Main;

import Atributo.Atributos;

public class Item {
	String name;
	public Atributos ElementalStats;
	public double Durabilidad = 100;
	double Peso = 5;

	Item(String vname, Atributos vElSt, double vDura, double vPeso) {
		name = vname;
		ElementalStats = vElSt;
		Durabilidad = vDura;
		Peso = vPeso;
	}

	void SwingAttack(Item Target) {
		Target.Durabilidad -= this.ElementalStats.Aplastante.Ataque / Target.ElementalStats.Aplastante.Defensa;
		Target.Durabilidad -= this.ElementalStats.Cortante.Ataque / Target.ElementalStats.Cortante.Defensa;
		System.out.println(Target.Durabilidad);
	}

}
