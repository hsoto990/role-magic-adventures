package Main;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import Atributo.Atributos;

public class Event {
	String Name = "";
	Point Pos = new Point(0, 0);
	int Altura = 0;
	String Tipe = "Wall";
	int speed = 4;
	int waitToMove = 0;
	String chara;
	char Dir = 'S';
	int IDtarget = -1;
	String mode = "Idle";
	ArrayList<String> WalkOn = new ArrayList<String>();
	ExtraVars EV = new ExtraVars();
	Inventory inven = new Inventory();
	Item itemOfEvent = new Item("Stick", new Atributos(), 100, 1);

	BufferedImage rima;

	Event(int x, int y, String vName) {
		Pos = new Point(x, y);
		Name = vName;
		setImage("BushDry");
	}

	Point PoseMod = new Point(0, 0);

	void Move() {
		if (itemOfEvent.Durabilidad < 0) {
			if (Name == "Drop") {
				DrawRect.eva.get(0).inven.inven.add(itemOfEvent);
				DrawRect.BanList.add(DrawRect.ivan);
			} else {
				setImage("Drops/tree");
				Name = "Drop";
				itemOfEvent.Durabilidad = 0;

			}
		}

		if (waitToMove == 0) {
			if (mode == "Atack") {
				if (Dir == 'N')
					DrawRect.DamMap[Pos.x][Pos.y - 1] = 50;
				else if (Dir == 'W')
					DrawRect.DamMap[Pos.x - 1][Pos.y] = 50;
				else if (Dir == 'S')
					DrawRect.DamMap[Pos.x][Pos.y + 1] = 50;
				else if (Dir == 'E')
					DrawRect.DamMap[Pos.x + 1][Pos.y] = 50;
				mode = "Idle";
			}
			if (mode == "Follow") {
				Follow();
				Roam();
			}
			if (mode == "Roam")
				Roam();

			if (mode == "Vanishing")
				// itemOfEvent.Durability--;

				movpa();
		} else
			waitToMove--;

	}

	void Follow() {
		if (IDtarget == -1)
			mode = "Idle";
		else {
			Event target = DrawRect.eva.coll.get(IDtarget);
			if (Pos.x > target.Pos.x)
				PoseMod.x -= 1;
			else if (Pos.x < target.Pos.x)
				PoseMod.x += 1;
			if (Pos.y > target.Pos.y)
				PoseMod.y -= 1;
			else if (Pos.y < target.Pos.y)
				PoseMod.y += 1;
		}
	}

	void Roam() {
		PoseMod.x += (int) (Math.random() * 4 - 2);
		PoseMod.y += (int) (Math.random() * 4 - 2);
	}

	void movpa() {
		int xpo = Pos.x + PoseMod.x;
		int ypo = Pos.y + PoseMod.y;
		if (WalkOn.contains(DrawRect.matip[xpo][ypo])) {
			Pos.x = xpo;
			Pos.y = ypo;
		} else if (WalkOn.contains(DrawRect.matip[xpo][Pos.y])) {
			Pos.x = xpo;
		} else if (WalkOn.contains(DrawRect.matip[Pos.x][ypo])) {
			Pos.y = ypo;
		}

		PoseMod = new Point(0, 0);
		waitToMove = speed;
	}

	void setImage(String trica) {
		try {
			rima = ImageIO.read(new File("/home/ceibal/Escritorio/LW RES/" + trica + ".png"));
		} catch (IOException e) {
			System.out.println("Textura 'tree' no encontrada");
		}
	}

	void PickUp() {
		// String newitemname = "";
		// newitemname+=(int)taaaa.thickness +"tk, ";
		// newitemname+=(int)taaaa.height +"tl, ";
		// newitemname += "Stick";
		// DrawRect.inven.add(newitemname, taaaa);
		// DrawRect.BanList.add(DrawRect.ivan);
	}
}
