package Main;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Tile {
	BufferedImage na;
	String Tipe = "Ground";
	Tile(String vna,String vTipe) {
		try {
			na = ImageIO.read(new File("/home/ceibal/Escritorio/LW RES/" + vna + ".png"));
		} catch (IOException e) {
			System.out.println("Textura '" + vna + "' no encontrada");
		}
		Tipe=vTipe;
	}
	public BufferedImage getColor() {
		return na;
	}
}
