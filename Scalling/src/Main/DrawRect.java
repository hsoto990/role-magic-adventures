package Main;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.*;

import Atributo.Atributos;

import java.awt.event.KeyAdapter;

public class DrawRect extends JPanel {
	public DrawRect() {
		// NOTHINGNESS
	}

	private static final long serialVersionUID = 1L;
	private static final int RECT_X = 0;
	private static final int RECT_Y = RECT_X;
	private static final int RECT_WIDTH = 530;
	private static final int RECT_HEIGHT = RECT_WIDTH;

	static Point playerPos = new Point(5, 5);
	static boolean showinven = false;

	static Graphics ham;
	int TSi = 32;
	boolean vvaaaaa = true;

	static int ivan = 0;
	static ArrayList<ArrayList<Integer>> Order = new ArrayList<ArrayList<Integer>>();

	@Override
	protected void paintComponent(Graphics g) {
		if (showinven) {
			eva.get(0).inven.show(g);
		} else {
			super.paintComponent(g);
			// draw the rectangle here
			int xcent = 8 * TSi - eva.get(0).Pos.x * TSi;
			int ycent = 8 * TSi - eva.get(0).Pos.y * TSi;
			for (int ix = 0; ix < 34; ix++) {
				for (int iy = 0; iy < 34; iy++) {
					g.drawImage(tila.getTileColor(ix, iy), ix * TSi + xcent, iy * TSi + ycent, TSi, TSi, null);
				}

			}
			for (int i = 0; i <= eva.last(); i++)
				Order.get(eva.get(i).Pos.y).add(i);
			for (int i = 0; i < Order.size(); i++)
				for (int i2 = 0; i2 < Order.get(i).size(); i2++) {
					Event evi = eva.get(Order.get(i).get(i2));
					g.drawImage(evi.rima, evi.Pos.x * TSi + xcent + 16 - evi.rima.getWidth() / 2,
							evi.Pos.y * TSi + ycent - evi.rima.getHeight() + 32, evi.rima.getWidth(),
							evi.rima.getHeight(), null);
				}

		}
	}

	@Override
	public Dimension getPreferredSize() {
		// so that our GUI is big enough
		return new Dimension(RECT_WIDTH + 2 * RECT_X, RECT_HEIGHT + 2 * RECT_Y);
	}

	public static JFrame frame = new JFrame("DrawRect");

	public static ArrayList<Integer> BanList = new ArrayList<Integer>();

	static void timerendo() {
		while (true) {
			Order.clear();
			for (int i = 0; i < 34; i++)
				Order.add(new ArrayList<Integer>());
			for (int i = BanList.size() - 1; i >= 0; i--)
				eva.coll.remove((int) BanList.get(i));

			BanList.clear();

			restartColision();

			for (ivan = 0; ivan <= eva.last(); ivan++) {
				eva.get(ivan).Move();
				// if (eva.get(i).Durability <= 0)
				// BanList.add(i);
			}
			for (Event ieva : eva.coll) {
				if (DamMap[ieva.Pos.x][ieva.Pos.y] != 0) {
					eva.get(0).inven.Equiped.SwingAttack(ieva.itemOfEvent);
				}
			}
			for (int ix = 0; ix < 34; ix++)
				for (int iy = 0; iy < 34; iy++)
					DamMap[ix][iy] = 0;
			frame.repaint();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
		}
	}

	static TileLayer tila = new TileLayer(34, 34);
	static EventArray eva = new EventArray();
	static String[][] matip = new String[34][34];
	static int[][] DamMap = new int[34][34];

	static ArrayList<BufferedImage> chara = new ArrayList<BufferedImage>();

	// create the GUI explicitly on the Swing event thread
	private static void createAndShowGui() {
		MaterialList.init();

		try {
			chara.add(ImageIO.read(new File("/home/ceibal/Escritorio/LW RES/Lis/N.gif")));
			chara.add(ImageIO.read(new File("/home/ceibal/Escritorio/LW RES/Lis/W.gif")));
			chara.add(ImageIO.read(new File("/home/ceibal/Escritorio/LW RES/Lis/S.gif")));
			chara.add(ImageIO.read(new File("/home/ceibal/Escritorio/LW RES/Lis/E.gif")));
		} catch (IOException e) {
		}
		for (int ix = 1; ix < 33; ix++)
			for (int iy = 1; iy < 20; iy++)
				tila.setTile(ix, iy, 1);
		for (int ix = 1; ix < 33; ix++)
			for (int iy = 20; iy < 25; iy++)
				tila.setTile(ix, iy, 2);
		for (int ix = 1; ix < 33; ix++)
			for (int iy = 25; iy < 33; iy++)
				tila.setTile(ix, iy, 3);
		for (int ix = 1; ix < 7; ix++)
			for (int iy = 1; iy < 7; iy++)
				tila.setTile(ix, iy, 4);
		eva.add(EventPrefabs.human(21, 8));
		eva.get(eva.last()).speed = 1;
		eva.get(eva.last()).rima = chara.get(2);
		eva.get(eva.last()).mode = "Player Controled";
		eva.get(eva.last()).Altura = 32;

		{
			Atributos tempAtrib = new Atributos();
			tempAtrib.Aplastante.Ataque = 10;
			eva.get(0).inven.add(new Item("Hammer", tempAtrib, 100, 1));
		}
		{
			Atributos tempAtrib = new Atributos();
			tempAtrib.Cortante.Ataque = 10;
			eva.get(0).inven.add(new Item("Axe", tempAtrib, 100, 1));
		}
		// eva.add(EventPrefabs.human(4, 9));
		// eva.get(eva.last()).IDtarget = 0;
		// eva.get(eva.last()).mode = "Follow";
		// eva.get(eva.last()).WalkOn.remove("Water");

		eva.add(new Event(4, 4, "Humanoid"));
		eva.get(eva.last()).setImage("TestChar/Fat");

		eva.add(new Event(6, 4, "Humanoid"));
		eva.get(eva.last()).setImage("TestChar/Undead Flesh");

		eva.add(new Event(8, 4, "Skely"));
		eva.get(eva.last()).setImage("TestChar/Skinny");
		eva.add(EventPrefabs.fish(28, 28));

		for (int i = 0; i < 20; i++) {
			for (int i2 = 0; i2 < 20; i2++) {
				eva.add(new Event(i, i2, "Skely"));
				eva.get(eva.last()).setImage("piu");
			}
		}
		{
			Point[] temp = { np(9, 3), np(14, 4), np(21, 4), np(28, 3), np(31, 5), np(4, 8), np(7, 9), np(14, 9),
					np(23, 7), np(29, 9), np(2, 10), np(13, 12), np(19, 12), np(8, 13), np(25, 13), np(3, 15),
					np(20, 16), np(7, 17) };
			for (Point tepa : temp) {
				eva.add(EventPrefabs.tree(tepa.x, tepa.y));
			}

		}
		{
			Point[] temp = { np(8, 6), np(14, 6), np(25, 4), np(17, 10), np(4, 12), np(28, 13), np(10, 15),
					np(15, 15) };
			for (Point tepa : temp)
				eva.add(EventPrefabs.rock(tepa.x, tepa.y));
		}

		restartColision();

		new Thread() {
			public void run() {
				timerendo();
			}
		}.start();

		DrawRect mainPanel = new DrawRect();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(mainPanel);
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		frame.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (showinven) {
					if (teclaPressed(arg0, "W"))
						if (eva.get(0).inven.selItem > 0)
							eva.get(0).inven.selItem--;
					if (teclaPressed(arg0, "S"))
						if (eva.get(0).inven.selItem < eva.get(0).inven.inven.size() - 1)
							eva.get(0).inven.selItem++;
					if (teclaPressed(arg0, "E"))
						eva.get(0).inven.equip();
					if (teclaPressed(arg0, "Q"))
						eva.get(0).inven.unequip();
				} else {
					if (eva.get(0).waitToMove == 0) {
						if (teclaPressed(arg0, "W")) {
							eva.get(0).Dir = 'N';
							eva.get(0).PoseMod.y = -1;
							eva.get(0).rima = chara.get(0);
						}
						if (teclaPressed(arg0, "A")) {
							eva.get(0).Dir = 'W';
							eva.get(0).PoseMod.x = -1;
							eva.get(0).rima = chara.get(1);
						}
						if (teclaPressed(arg0, "S")) {
							eva.get(0).Dir = 'S';
							eva.get(0).PoseMod.y = 1;
							eva.get(0).rima = chara.get(2);
						}
						if (teclaPressed(arg0, "D")) {
							eva.get(0).Dir = 'E';
							eva.get(0).PoseMod.x = 1;
							eva.get(0).rima = chara.get(3);
						}
						eva.get(0).movpa();

					}
					if (teclaPressed(arg0, "E")) {
						eva.get(0).mode = "Atack";
					}
				}
				if (teclaPressed(arg0, "I"))
					showinven = !showinven;
			}
		});
	}

	static Point np(int x, int y) {
		return new Point(x, y);
	}

	static void restartColision() {
		for (int ix = 0; ix < 34; ix++)
			for (int iy = 0; iy < 34; iy++)
				matip[ix][iy] = tila.tise.tilesa.get(tila.tila[ix][iy]).Tipe;
		for (Event i : eva.coll) {
			matip[i.Pos.x][i.Pos.y] = i.Tipe;
		}
	}

	static boolean teclaPressed(KeyEvent arg0, String w) {
		return ("" + arg0.getKeyChar()).equalsIgnoreCase(w);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGui();
			}
		});
	}
}