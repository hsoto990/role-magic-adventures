package rma;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class ImageToMap {
	static Map imageToMap() throws Exception {
		BufferedImage icomand = ImageIO.read(new File("/home/ceibal/Imágenes/Maaaap/zombimapaa.png"));
		Map tempMap = new Map(DrawRect.World.size(),icomand.getWidth(), icomand.getHeight());
		for (int ix = 0; ix < tempMap.widht; ix++) {
			tempMap.eventsInCuad.add(new ArrayList<ArrayList<Integer>>());
			for (int iy = 0; iy < tempMap.height; iy++) {
				tempMap.evtColision[ix][iy]=true;
				tempMap.eventsInCuad.get(ix).add(new ArrayList<Integer>());
				int icomandgetRGB = icomand.getRGB(ix, iy);
				double thisrand = Math.random();
				if (icomandgetRGB == new Color(175, 26, 25).getRGB())
					tempMap.grid[ix][iy].texture = "Nuclear/Grass";
				else if (icomandgetRGB == new Color(153, 132, 69).getRGB()) {
					if (thisrand < 0.01)
						tempMap.grid[ix][iy].texture = "DryLand/Tree";
					else if (thisrand < 0.01 + 0.15)
						tempMap.grid[ix][iy].texture = "DryLand/GrassMark";
					else
						tempMap.grid[ix][iy].texture = "DryLand/Grass";
				} else if (icomandgetRGB == new Color(44, 91, 54).getRGB()) {
					if (thisrand <= 0.16)
						tempMap.grid[ix][iy].texture = "Forest/Tree";
					else if (thisrand <= 0.16 + 0.09)
						tempMap.grid[ix][iy].texture = "Forest/Flower";
					else
						tempMap.grid[ix][iy].texture = "Forest/Grass";
				} else if (icomandgetRGB == new Color(67, 140, 65).getRGB()) {
					if (thisrand < 0.1)
						tempMap.grid[ix][iy].texture = "GrassLand/Tree";
					else if (thisrand < 0.17)
						tempMap.grid[ix][iy].texture = "GrassLand/GrassMark";
					else if (thisrand < 0.25)
						tempMap.grid[ix][iy].texture = "GrassLand/GrassPlant";
					else
						tempMap.grid[ix][iy].texture = "GrassLand/Grass";
				} else if (icomandgetRGB == new Color(39, 115, 84).getRGB())
					tempMap.grid[ix][iy].texture = "Taiga/Grass";
				else if (icomandgetRGB == new Color(255, 255, 255).getRGB())
					tempMap.grid[ix][iy].texture = "Tundra/Snow";
				else if (icomandgetRGB == new Color(65, 65, 65).getRGB())
					tempMap.grid[ix][iy].texture = "Burned/Grass";
				else if (icomandgetRGB == new Color(90, 90, 64).getRGB())
					tempMap.grid[ix][iy].texture = "Debris/Ground";
				else if (icomandgetRGB == new Color(250, 215, 150).getRGB())
					tempMap.grid[ix][iy].texture = "Desert/Ground";
				else if (icomandgetRGB == new Color(255, 178, 127).getRGB())
					tempMap.grid[ix][iy].texture = "General/Path";
				else if (icomandgetRGB == new Color(160, 160, 160).getRGB())
					tempMap.grid[ix][iy].texture = "General/RockFloor";
				else if (icomandgetRGB == new Color(127, 51, 0).getRGB()) {
					tempMap.grid[ix][iy].texture = "General/LogWall";
				} else if (icomandgetRGB == new Color(110, 140, 200).getRGB()) {
					tempMap.grid[ix][iy].texture = "Liquid/Water";
				} else if (icomandgetRGB == new Color(180, 255, 0).getRGB()) {
					tempMap.grid[ix][iy].texture = "Liquid/Waste";
				} else if (icomandgetRGB == new Color(127, 0, 0).getRGB()) {
					tempMap.grid[ix][iy].texture = "Nuclear/Border";
				}
			}
		}
		return tempMap;
	}

}
