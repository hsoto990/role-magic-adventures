package rma;

import java.awt.Point;
import java.util.ArrayList;

public class Event {
	int ID = 0;
	int inMap=0;
	Point pos;
	double durability;
	double MaxDur;

	public String Chara;
	public String Pose;
	ArrayList<String> tags = new ArrayList<String>();
	boolean solid;
	boolean standing = false;
	boolean lookwhenmoved = false;

	Event(int vID,int vmap,  int x, int y, double vdurability, String vChara, String vPose, boolean vphantom) {
		ID = vID;
		inMap=vmap;
		pos = new Point(x, y);
		durability = vdurability;
		Chara = vChara;
		Pose = vPose;
		solid = vphantom;
	}
	
	void lookAtMock(int xmod, int ymod) {
		if (lookwhenmoved) {
			if (ymod == 1)
				Pose=("S");
			else if (ymod == -1)
				Pose=("N");
			else if (xmod == 1)
				Pose=("E");
			else if (xmod == -1)
				Pose=("W");
		}
	}
	void Move(int xmod, int ymod, int time) {
		if (standing) {
			standing = false;
			lookAtMock(xmod, ymod);
			if (solid) {
				if (DrawRect.World.get(DrawRect.aMap).evtColision[pos.x + xmod] [pos.y + ymod]) {
					DrawRect.World.get(DrawRect.aMap).evtColision[pos.x][pos.y] = true;
					DrawRect.World.get(DrawRect.aMap).evtColision[pos.x + xmod][pos.y + ymod] = false;
					teleport(xmod, ymod);
				}
			} else
				teleport(xmod, ymod);
			standing = true;
		}
	}
	public void teleport(int x, int y) {
		DrawRect.World.get(inMap).eventsInCuad.get(pos.x).get(pos.y).remove((Object)ID);
		pos= new Point (x,y);
		DrawRect.World.get(inMap).eventsInCuad.get(x).get(y).add(ID);
	}
	void unsolidify() {
		if (solid) {
			solid = false;
			DrawRect.World.get(inMap).evtColision[pos.x][pos.y] = true;
		}
	}
}
