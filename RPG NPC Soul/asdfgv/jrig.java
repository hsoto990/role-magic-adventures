package rma;

import java.awt.Point;
import java.util.ArrayList;

public class jrig {
	int thisEventID = 1;

	void esperar(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}

	Event player() {
		return DrawRect.World.get(DrawRect.aMap).events.get(0);
	}

	Event hshe() {
		return DrawRect.World.get(DrawRect.aMap).events.get(thisEventID);
	}

	double damage() {
		return hshe().MaxDur - hshe().durability;
	}
	
	static ArrayList<Point> setfinishAllWalkable() {
		ArrayList<Point> finis = new ArrayList<Point>();
		for (int x = 0; x < dasdf.mapXsize; x++)
			for (int y = 0; y < dasdf.mapYsize; y++)
				if (dasdf.IsPassable(x, y))
					finis.add(new Point(x, y));
		return finis;
	}

	boolean canmorunfoplayer(int extraDis) {
		ArrayList<Point> finishes = new ArrayList<Point>();
		finishes = AIBehavior.setfinishAllWalkable();
		int dis = new GetDistance().resolve(hshe().pos, player().pos) + extraDis;

		for (int i = -dis; i <= dis; i++)
			for (int i2 = -dis; i2 <= dis; i2++)
				finishes.remove(new Point(player().pos.x + i, player().pos.y + i2));

		if (new GetDistance().resolve2(hshe().pos, finishes) == -1)

			return false;
		else
			return true;
	}

	void morunfoplayer(int extraDis, int Speed) {
		ArrayList<Point> finishes = new ArrayList<Point>();
		finishes = AIBehavior.setfinishAllWalkable();
		int dis = new GetDistance().resolve(hshe().pos, player().pos) + extraDis;
		for (int i = -dis; i <= dis; i++)
			for (int i2 = -dis; i2 <= dis; i2++)
				finishes.remove(new Point(player().pos.x + i, player().pos.y + i2));
		new SinglePF().resolve(thisEventID, finishes, Speed);
	}

	boolean can_move_toward_player() {
		if (new GetDistance().resolve(hshe().pos, player().pos) == -1)
			return false;
		else
			return true;
	}

	void move_toward_player() {
		ArrayList<Point> finishes = new ArrayList<Point>();
		finishes.add(player().pos);
		new SinglePF().resolve(thisEventID, finishes, 30);
	}

	boolean can_move_away_from_player() {
		return canmorunfoplayer(3);
	}

	void move_away_from_player() {
		morunfoplayer(3, 25);
	}

	boolean can_run_away_from_player() {
		return canmorunfoplayer(10);
	}

	void run_away_from_player() {
		morunfoplayer(10, 15);
	}

	boolean can_attack_player() {
		int pdis = AIBehavior.getDis(hshe().pos, player().pos);
		if (pdis >= AtackRange[0] & pdis <= AtackRange[1])
			return true;
		else
			return false;
	}

	void attack_player() {
		hshe().standing = false;
		String actmov = hshe().EventPose;
		hshe().setPoses("Atk");
		Evt.esperar(500);
		hshe().setPoses(actmov);
		esperar(100);
		hshe().standing = true;
	}

	void death() {
		hshe().setPoses("Dead");
		hshe().unsolidify();
	}

	boolean too_far_from_player() {
		int pdis = AIBehavior.getDis(hshe().pos, player().pos);
		if (pdis > AtackRange[1])
			return true;
		else
			return false;
	}

	boolean too_close_to_player() {
		int pdis = AIBehavior.getDis(hshe().pos, player().pos);
		if (pdis < AtackRange[0])
			return true;
		else
			return false;
	}

	void stand_still() {
		int x = (int) (random() * 4 - 2);
		int y = (int) (random() * 4 - 2);
		dasdf.ev.events.get(thisEventID).Move(x, y, 100);
	}

	void wait_to_heal() {
		esperar(10);
		hshe().damage--;
	}

	boolean is_player_detected() {
		if (Math.hypot(player().pos.x - hshe().pos.x, player().pos.y - hshe().pos.y) <= 10)
			return true;
		else
			return false;
	}

	double random() {
		return Math.random();
	}

	int[] AtackRange = { 2, 3 };
	double Maxlife = 100;
	double morale = 0;

	void AIGeneric(int vthisEventID, int vMaxlife, int vmorale, int[] vAtackRange) {
		new Thread() {
			public void run() {
				thisEventID = vthisEventID;
				Maxlife = vMaxlife;
				morale = vmorale;
				AtackRange = vAtackRange;

				while (damage() < Maxlife) {
					if (damage() > morale) {
						if (is_player_detected()) {
							if (can_run_away_from_player())
								run_away_from_player();
							else if (can_attack_player())
								attack_player();
							else if (too_close_to_player() & can_move_away_from_player())
								morunfoplayer(2, 15);
							else
								wait_to_heal();
						} else
							wait_to_heal();
					} else 						if (is_player_detected()) {
 if (can_attack_player()) {
						attack_player();
					} else if (too_far_from_player() & can_move_toward_player())
						move_toward_player();
					else if (too_close_to_player() & can_move_away_from_player())
						morunfoplayer(2, 17);
					else
						stand_still();
					}else
						stand_still();
				}
				death();
			}
		}.start();
	}
}
