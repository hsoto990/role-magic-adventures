package rma;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.*;

public class DrawRect extends JPanel {
	private static final long serialVersionUID = 1L;
	private static final int RECT_X = 0;
	private static final int RECT_Y = RECT_X;
	private static final int RECT_WIDTH = 530;
	private static final int RECT_HEIGHT = RECT_WIDTH;

	public static ArrayList<Map> World = new ArrayList<Map>();
	public static int aMap = 0;
	static Point playerPos = new Point(5, 5);

	static Graphics ham;

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		// draw the rectangle here
		g.drawRect(RECT_X, RECT_Y, RECT_WIDTH, RECT_HEIGHT);
		// while (true) {
		Map jia = World.get(0);

		for (int ix = 0; ix <= 16; ix++) {
			int sumX = ix - 8 + playerPos.x;
			if (sumX >= 0 & sumX < jia.widht)
				for (int iy = 0; iy <= 16; iy++) {
					int sumY = iy - 8 + playerPos.y;
					if (sumY >= 0 & sumY < jia.height) {
						try {
							g.drawImage(ImageIO.read(new File("/home/ceibal/Imágenes/Maaaap/Graph/Tiles/"
									+ jia.grid[sumX][sumY].texture + ".png")), ix * 32, iy * 32, 32, 32, null);
							ArrayList<Integer> jaaaa = jia.eventsInCuad.get(playerPos.x+ix -8).get(playerPos.y+iy-8);
							for (int ie = 0; ie < jaaaa.size(); ie++)
								g.drawImage(
										ImageIO.read(new File("/home/ceibal/Imágenes/Maaaap/Graph/Charas/"
												+ jia.events.get(jaaaa.get(ie)).Chara + "/"
												+ jia.events.get(jaaaa.get(ie)).Pose + ".png")),
										ix * 32, iy * 32, 32, 32, null);
//							g.drawImage(ImageIO.read(new File("/home/ceibal/Imágenes/Maaaap/Graph/Seemark.png")), 0, 0, 530, 530, null);
						} catch (IOException e) {
						}
					} else {
						g.setColor(Color.black);
						g.fillRect(ix * 32, iy * 32, 32, 32);
					}
				}
			else {
				g.setColor(Color.black);
				for (int iy = 0; iy <= 16; iy++)
					g.fillRect(ix * 32, iy * 32, 32, 32);
			}
		}
		// }
	}

	@Override
	public Dimension getPreferredSize() {
		// so that our GUI is big enough
		return new Dimension(RECT_WIDTH + 2 * RECT_X, RECT_HEIGHT + 2 * RECT_Y);
	}

	// create the GUI explicitly on the Swing event thread
	private static void createAndShowGui() {
		try {
			World.add(ImageToMap.imageToMap());
		} catch (Exception e1) {
		}
		World.get(0).addEvent(playerPos.x, playerPos.y, 100, "Me", "S", false);
		
		World.get(0).addEvent(5, 10, 100, "Zombie", "S", false);

		DrawRect mainPanel = new DrawRect();

		JFrame frame = new JFrame("DrawRect");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(mainPanel);
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
		frame.addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (("" + e.getKeyChar()).equalsIgnoreCase("W"))
					playerPos.y--;
				else if (("" + e.getKeyChar()).equalsIgnoreCase("A"))
					playerPos.x--;
				else if (("" + e.getKeyChar()).equalsIgnoreCase("S"))
					playerPos.y++;
				else if (("" + e.getKeyChar()).equalsIgnoreCase("D"))
					playerPos.x++;
				World.get(0).events.get(0).teleport(playerPos.x, playerPos.y);
				frame.repaint();
			}

			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
			}
		});
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGui();
			}
		});
	}
}