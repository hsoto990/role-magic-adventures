package rma;

import java.awt.Point;
import java.util.ArrayList;

public class GetDistance {
	// GENERAL VARIABLES

	// private static int cells = 120;

	Point finpoint = new Point();
	// final static int MSIZE = 600;
	int CSIZE = 16;
	int ActionString = -1;

	// BOOLEANS
	private boolean solving = false;
	
	// UTIL
	Node[][] map;
	Algorithm Alg = new Algorithm();
	int catualEven = 0;

	public void mainu() { // MAIN METHOD
		new GetDistance();
	}
	int heiggg=DrawRect.World.get(DrawRect.aMap).height;
	int wighhh=DrawRect.World.get(DrawRect.aMap).widht;
	public GetDistance() { // CONSTRUCTOR
		clearMap();
		for (int iy = 0; iy < heiggg; iy++)
			for (int ix = 0; ix <  wighhh; ix++)
				if (!(DrawRect.World.get(DrawRect.aMap).evtColision[ix][iy]))
					map[ix][iy].cellType = 2;

		
	}

	public void clearMap() { // CLEAR MAP
		map = new Node[wighhh][heiggg]; // CREATE NEW MAP OF NODES
		for (int x = 0; x < wighhh; x++) {
			for (int y = 0; y < heiggg; y++) {
				map[x][y] = new Node(3, x, y); // SET ALL NODES TO EMPTY
			}
		}
		solving = false;
	}

	public void resetMap() { // RESET MAP
		for (int x = 0; x < wighhh; x++) {
			for (int y = 0; y < heiggg; y++) {
				Node current = map[x][y];
				// CHECK TO SEE IF CURRENT NODE IS EITHER CHECKED OR FINAL PATH
				if (current.cellType != 2)
					map[x][y] = new Node(3, x, y); // RESET IT TO AN EMPTY NODE
			}
		}


		// if (finishx > -1 && finishy > -1)
		// map[finishx][finishy] = new Node(1, finishx, finishy);
		solving = false;
	}

	int disa = -1;

	public int resolve(Point startt, Point finishh) {

		mainu();


		solving = false;
		try {
			Thread.sleep(30);
		} catch (InterruptedException e) {
		}

			resetMap(); // RESET THE MAP WHENEVER CLICKED
			map[startt.x][startt.y].cellType = 0;
			map[startt.x][startt.y].hops = 0;
			map[finishh.x][finishh.y].cellType = 1;
			map[finishh.x][finishh.y].hops = -1;

			solving = true;
			new Algorithm().Dijkstra(startt);
			return disa;
	}

	public int resolve2(Point startt, ArrayList<Point> finishh) {

		mainu();


		solving = false;
		try {
			Thread.sleep(30);
		} catch (InterruptedException e) {
		}

			resetMap(); // RESET THE MAP WHENEVER CLICKED
			map[startt.x][startt.y].cellType = 0;
			map[startt.x][startt.y].hops = 0;
			for (int i=0;i<finishh.size();i++) {
			map[finishh.get(i).x][finishh.get(i).y].cellType = 1;
			map[finishh.get(i).x][finishh.get(i).y].hops = -1;
			}
			solving = true;
			new Algorithm().Dijkstra(startt);
			return disa;
	}
	
	class Algorithm { // ALGORITHM CLASS

		public void Dijkstra(Point startt) {
			ArrayList<Node> priority = new ArrayList<Node>(); // CREATE A PRIORITY QUE
			priority.add(map[startt.x][startt.y]); // ADD THE START TO THE QUE
			while (solving) {
				if (priority.size() <= 0) { // IF THE QUE IS 0 THEN NO PATH CAN BE FOUND
					solving = false;
					break;
				}
				int hops = priority.get(0).hops + 1; // INCREMENT THE HOPS VARIABLE
				ArrayList<Node> explored = exploreNeighbors(priority.get(0), hops); // CREATE AN ARRAYLIST OF
																								// NODES THAT
				// WERE EXPLORED
				if (explored.size() > 0) {
					priority.remove(0); // REMOVE THE NODE FROM THE QUE
					priority.addAll(explored); // ADD ALL THE NEW NODES TO THE QUE
				} else { // IF NO NODES WERE EXPLORED THEN JUST REMOVE THE NODE FROM THE QUE
					priority.remove(0);
				}
			}

		}

		public ArrayList<Node> exploreNeighbors(Node current, int hops) { // EXPLORE NEIGHBORS
			ArrayList<Node> explored = new ArrayList<Node>(); // LIST OF NODES THAT HAVE BEEN EXPLORED
			checkNeigh(current, hops, -1, 0, explored);
			checkNeigh(current, hops, +1, 0, explored);
			checkNeigh(current, hops, 0, -1, explored);
			checkNeigh(current, hops, 0, +1, explored);

			checkNeigh(current, hops, -1, -1, explored);
			checkNeigh(current, hops, +1, +1, explored);
			checkNeigh(current, hops, +1, -1, explored);
			checkNeigh(current, hops, -1, +1, explored);
			return explored;
		}

		void checkNeigh(Node current, int hops, int a, int b, ArrayList<Node> explored) {
			int xbound = current.x + a;
			int ybound = current.y + b;
			// MAKES SURE THE NODE IS NOT OUTSIDE THE GRID
			if ((xbound > -1) && (ybound > -1)) {
				Node neighbor = map[xbound][ybound];
				// CHECKS IF THE NODE IS NOT A WALL AND THAT IT HAS NOT BEEN EXPLORED
				if ((neighbor.hops == -1 || neighbor.hops > hops) && neighbor.cellType != 2) {
					explore(neighbor, current.x, current.y, hops); // EXPLORE THE NODE
					explored.add(neighbor); // ADD THE NODE TO THE LIST
				}
			}
		}


		public void explore(Node current, int lastx, int lasty, int hops) { // EXPLORE A NODE
			if (current.cellType != 0 && current.cellType != 1) // CHECK THAT THE NODE IS NOT THE START OR FINISH
				current.cellType = 4; // SET IT TO EXPLORED
			current.hops = hops; // SET THE HOPS FROM THE START
			if (current.cellType == 1) { // IF THE NODE IS THE FINISH THEN BACKTRACK TO GET THE PATH
				finpoint = new Point(current.x, current.y);
				disa = hops;
			}
		}

	}

	class Node {

		// 0 = start, 1 = finish, 2 = wall, 3 = empty, 4 = checked, 5 = finalpath
		private int cellType = 0;
		private int hops;
		private int x;
		private int y;

		public Node(int type, int x, int y) { // CONSTRUCTOR
			cellType = type;
			this.x = x;
			this.y = y;
			hops = -1;
		}

	}
}