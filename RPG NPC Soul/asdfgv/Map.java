package rma;

import java.awt.Point;
import java.util.ArrayList;

public class Map {
	int ID=0;
	public int widht=17;
	public int height=17;
	Tile[][] grid = new Tile[widht][height];
	ArrayList<Event>events = new ArrayList<Event>();
	ArrayList<ArrayList<ArrayList<Integer>>>eventsInCuad = new ArrayList<ArrayList<ArrayList<Integer>>>();
	
	boolean[][] evtColision = new boolean[widht][height];
	
	Map (int vID,int w,int h) {
		ID=vID;
		widht=w;
		height=h;
		grid = new Tile[widht][height];
		for (int ix=0;ix<widht;ix++)
			for (int iy=0;iy<height;iy++)
				grid[ix][iy]=new Tile("","GrassLand/Grass");
	}
	public void addEvent(int x, int y, double vdurability, String vChara, String vPose, boolean vphantom) {
		eventsInCuad.get(x).get(y).add(events.size());
		events.add(new Event(events.size(),ID,x,y,vdurability,vChara,vPose,vphantom));
	}
}
