package rma;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Point;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class dasdf extends JFrame {

	static int mapXsize = 25;
	static int mapYsize = 13;
	String[][] maplayer = new String[mapXsize][mapYsize];
	JLabel[][] maplabel = new JLabel[mapXsize][mapYsize];
	static boolean[][] mapColision = new boolean[mapXsize][mapYsize];
	static boolean[][] evtColision = new boolean[mapXsize][mapYsize];

	static JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					dasdf frame = new dasdf();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	static Events ev = new Events();

	/**
	 * Create the frame.
	 */
	public dasdf() {

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				new Thread() {
					public void run() {

						if (arg0.getKeyChar() == 'a')
							ev.events.get(0).Move(-1, 0, 15);
						else if (arg0.getKeyChar() == 'd')
							ev.events.get(0).Move(1, 0, 15);
						else if (arg0.getKeyChar() == 'w')
							ev.events.get(0).Move(0, -1, 15);
						else if (arg0.getKeyChar() == 's')
							ev.events.get(0).Move(0, 1, 15);

						if (arg0.getKeyChar() == 'A')
							ev.events.get(0).Move(-1, 0, 7);
						else if (arg0.getKeyChar() == 'D')
							ev.events.get(0).Move(1, 0, 7);
						else if (arg0.getKeyChar() == 'W')
							ev.events.get(0).Move(0, -1, 7);
						else if (arg0.getKeyChar() == 'S')
							ev.events.get(0).Move(0, 1, 7);

					}

				}.start();
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(25 * 32, 13 * 32+26);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		clearColishionEv();

		// Player
		ev.add("Me", "S", 5, 10, true, true);
		ev.events.get(ev.last).tags="Human";

		// Lis
		ev.add("Zombie", "W", 8, 10, true, true);
		ev.events.get(ev.last).tags="Zombie";
		ev.add("Zombie", "W", 9, 10, true, true);
		ev.events.get(ev.last).tags="Zombie";
		ev.add("Zombie", "W", 10, 10, true, true);
		ev.events.get(ev.last).tags="Zombie";
		ev.add("Zombie", "N", 9, 9, true, true);
		ev.events.get(ev.last).tags="Zombie";
		ev.add("Zombie", "N", 9, 8, true, true);
		ev.events.get(ev.last).tags="Zombie";
		ev.add("Zombie", "N", 9, 7, true, true);
		ev.events.get(ev.last).tags="Zombie";
		ev.add("Guard", "N", 9, 6, true, true);
		ev.events.get(ev.last).tags="Human";

		// Events
		for (int i = 0; i < mapXsize; i++) {
			ev.add("Obstacles", "Tree", i, 0, false, true);
			ev.add("Obstacles", "Tree", i, mapYsize - 1, false, true);
		}
		for (int i = 1; i < mapYsize - 1; i++) {
			ev.add("Obstacles", "Tree", 0, i, false, true);
			ev.add("Obstacles", "Tree", mapXsize - 1, i, false, true);
		}

		ev.add("Obstacles", "Tree", 11, 6, false, true);
		ev.add("Obstacles", "Tree", 11, 7, false, true);
		ev.add("Obstacles", "Tree", 11, 8, false, true);
		ev.add("Obstacles", "Tree", 11, 9, false, true);
		ev.add("Obstacles", "Tree", 11, 10, false, true);
		ev.add("Obstacles", "Tree", 11, 11, false, true);

		ev.add("Obstacles", "Meat", 5, 3, false, false);

		initMap();

		ev.startin();
	}

	public static boolean IsPassable(int x, int y) {
		return (evtColision[x][y]) & (mapColision[x][y]);
	}

	void initMap() {
		for (int iy = 0; iy < mapYsize; iy++)
			for (int ix = 0; ix < mapXsize; ix++) {
				maplabel[ix][iy] = new JLabel("");
				maplayer[ix][iy] = "Grass";
				maplabel[ix][iy]
						.setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/" + maplayer[ix][iy] + ".png"));
				contentPane.add(maplabel[ix][iy]);
				maplabel[ix][iy].setBounds(ix * 32, iy * 32, 32, 32);
				mapColision[ix][iy] = true;
			}
	}

	void clearColishionEv() {
		for (int iy = 0; iy < mapYsize; iy++)
			for (int ix = 0; ix < mapXsize; ix++)
				evtColision[ix][iy] = true;
	}

}
