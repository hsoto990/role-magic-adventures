package rma;

import java.awt.Point;
import java.util.ArrayList;

public class AIBehavior {

	static int getDis(Point i1, Point i2) {
		return new GetDistance().resolve(i1, i2);
	}

	static void esperar(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}

	static ArrayList<Point> finisharround(int x, int y) {
		ArrayList<Point> finishes = new ArrayList<Point>();
		finishes.add(new Point(x, y));
		// finishes.add(new Point(x, y - 1));
		// finishes.add(new Point(x + 1, y - 1));
		// finishes.add(new Point(x - 1, y));
		// finishes.add(new Point(x + 1, y));
		// finishes.add(new Point(x - 1, y + 1));
		// finishes.add(new Point(x, y + 1));
		// finishes.add(new Point(x + 1, y + 1));
		return finishes;

	}

	static void atak(Evt even) {
		even.standing = false;
		String actmov = even.EventPose;
		even.setPoses("Atk");
		Evt.esperar(500);
		even.setPoses(actmov);
		esperar(100);
		even.standing = true;
	}

	static void AIZombie(Evt even) {
		new Thread() {
			public void run() {
				while (true) {
					if (even.standing) {
						int disToPlayer = getDis(even.pos, dasdf.ev.events.get(0).pos);
						if (disToPlayer == 1) {
							atak(even);
						} else if (disToPlayer < 10) {
							ArrayList<Point> finishes = new ArrayList<Point>();
							finishes.add(dasdf.ev.events.get(0).pos);
							new SinglePF().resolve(even.ID, finishes);

						} else {
							int x = (int) (Math.random() * 4 - 2);
							int y = (int) (Math.random() * 4 - 2);
							dasdf.ev.events.get(even.ID).Move(x, y, 100);
						}
					}
				}
			}
		}.start();
	}

	static ArrayList<Evt> FiltrarPorTag(ArrayList<Evt> montcOri, String tag) {
		ArrayList<Evt> montc = new ArrayList<Evt>();
		for (int i=0;i< montcOri.size();i++) {
			if (montcOri.get(i).tags.equals(tag))
				montc.add(montcOri.get(i));
		}
		return montc;
	}
	
	static Evt nearestEvt(Point even,ArrayList<Evt> montc) {
		int minID=0;
		int minDis=getDis(even,montc.get(0).pos);
		for (int i=1;i<montc.size();i++) {
			int thisdis=getDis(even,montc.get(i).pos);
			if (thisdis<minDis) {
				minDis=thisdis;
				minID=i;
			}
		}
		return montc.get(minID);
	}

	static void AIGuard(Evt even) {
		new Thread() {
			public void run() {
				while (true) {
					if (even.standing) {
						ArrayList<Evt> montc = dasdf.ev.events;
						montc = FiltrarPorTag(montc,"Zombie");
						Evt tarj = nearestEvt(even.pos,montc);
						int disToPlayer = getDis(even.pos, tarj.pos);
						if (disToPlayer == 1) {
							atak(even);
						} else if (disToPlayer < 10) {
							ArrayList<Point> finishes = new ArrayList<Point>();
							finishes.add(tarj.pos);
							new SinglePF().resolve(even.ID, finishes);

						} else {
							int x = (int) (Math.random() * 4 - 2);
							int y = (int) (Math.random() * 4 - 2);
							dasdf.ev.events.get(even.ID).Move(x, y, 100);
						}
					}
				}
			}
		}.start();
	}
}
