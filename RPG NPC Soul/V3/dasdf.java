package rma;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Point;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class dasdf extends JFrame {

	static int mapXsize = 25;
	static int mapYsize = 13;
	String[][] maplayer = new String[mapXsize][mapYsize];
	JLabel[][] maplabel = new JLabel[mapXsize][mapYsize];
	static boolean[][] mapColision = new boolean[mapXsize][mapYsize];
	static boolean[][] evtColision = new boolean[mapXsize][mapYsize];
	static JPanel mapPane = new JPanel();
	static JLabel HubLabel = new JLabel();

	static JPanel contentPane;

	/**
	 * Launch the application.
	 */
	static dasdf frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new dasdf();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	static Events ev = new Events();

	/**
	 * Create the frame.
	 * 
	 * @return
	 */

	static void repant() {
		new Thread() {
			public void run() {
				while (true) {
					contentPane.repaint();
				}
			}
		}.start();
	}

	static void playerMove(int xmod, int ymod, int time) {
		Evt pla = ev.events.get(0);
		if (pla.standing) {
			ev.events.get(0).standing = false;
			ev.events.get(0).lookAtMock(xmod, ymod);
			if (dasdf.IsPassable(pla.pos.x + xmod, pla.pos.y + ymod)) {
				dasdf.evtColision[pla.pos.x][pla.pos.y] = true;
				dasdf.evtColision[pla.pos.x + xmod][pla.pos.y + ymod] = false;
				ev.events.get(0).pos.x += xmod;
				ev.events.get(0).pos.y += ymod;
				for (int i = 0; i < 16; i++) {
					ev.events.get(0).Eventlabel.setLocation(ev.events.get(0).Eventlabel.getX() + xmod * 2,
							ev.events.get(0).Eventlabel.getY() + ymod * 2);
					mapPane.setLocation(mapPane.getLocation().x - 2 * xmod, mapPane.getLocation().y - 2 * ymod);
					try {
						Thread.sleep(time);
					} catch (InterruptedException e) {
					}
				}
				ev.events.get(0).Eventlabel.setLocation(ev.events.get(0).pos.x * 32, ev.events.get(0).pos.y * 32);
			}
			ev.events.get(0).standing = true;
		}
	}

	public dasdf() {

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				new Thread() {
					public void run() {

						if (arg0.getKeyChar() == 'a')
							playerMove(-1, 0, 15);
						else if (arg0.getKeyChar() == 'd')
							playerMove(1, 0, 15);
						else if (arg0.getKeyChar() == 'w')
							playerMove(0, -1, 15);
						else if (arg0.getKeyChar() == 's')
							playerMove(0, 1, 15);

						else if (arg0.getKeyChar() == 'A')
							playerMove(-1, 0, 7);
						else if (arg0.getKeyChar() == 'D')
							playerMove(1, 0, 7);
						else if (arg0.getKeyChar() == 'W')
							playerMove(0, -1, 7);
						else if (arg0.getKeyChar() == 'S')
							playerMove(0, 1, 7);

						else if (arg0.getKeyChar() == 'e')
							ev.events.get(1).damage += 40;
					}

				}.start();
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(530, 530);
		setUndecorated(true);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		HubLabel.setBounds(0, 0, 530, 530);
		HubLabel.setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Seemark.png"));
		HubLabel.setLayout(new BorderLayout(0, 0));
		contentPane.add(HubLabel);
		mapPane.setBounds(3 * 32 + 32 / 2, -3 * 32 + 32 / 2, 10000, 10000);
		mapPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(mapPane);

		try {
			BufferedImage icomand = ImageIO.read(new File("/home/ceibal/Imágenes/Maaaap/zombimapaa.png"));
			mapYsize = icomand.getHeight();
			mapXsize = icomand.getWidth();
			maplayer = new String[mapXsize][mapYsize];
			maplabel = new JLabel[mapXsize][mapYsize];
			mapColision = new boolean[mapXsize][mapYsize];
			evtColision = new boolean[mapXsize][mapYsize];
		} catch (Exception e) {
		}

		clearColishionEv();

		// Player
		ev.add("Me", "S", 5, 10, true, true);
		// ev.events.get(ev.last).tags="Human";
		ev.events.get(ev.last).tags = "Player";

		// Lis
		// ev.add("Zombie", "W", 8, 10, true, true);
		//// ev.events.get(ev.last).tags="Zombie";
		// ev.add("Zombie", "W", 9, 10, true, true);
		//// ev.events.get(ev.last).tags="Zombie";
		// ev.add("Zombie", "W", 10, 10, true, true);
		// ev.events.get(ev.last).tags="Zombie";
		// ev.add("Zombie", "N", 9, 9, true, true);
		// ev.events.get(ev.last).tags="Zombie";
		// ev.add("Zombie", "N", 9, 8, true, true);
		// ev.events.get(ev.last).tags="Zombie";
		// ev.add("Zombie", "N", 9, 7, true, true);
		// ev.events.get(ev.last).tags="Zombie";
		// ev.add("Guard", "N", 9, 6, true, true);
		//// ev.events.get(ev.last).tags="Human";

		ev.add("Zombie", "N", 6, 6, true, true);
		ev.events.get(ev.last).tags = "Zombi";
		new jrig().AIGeneric(ev.last, 100, 50, new int[] { 1, 1 });

		ev.add("Zombie", "N", 5, 6, true, true);
		ev.events.get(ev.last).tags = "Zombi";
		new jrig().AIGeneric(ev.last, 100, 50, new int[] { 1, 1 });
		
		ev.add("null", "N", 0, 0, true, true);
		
		repant();
		
		new Thread() {
			public void run() {
				SpawnAllZombies();
				initMap();
			}
		}.start();
	}

	public static boolean IsPassable(int x, int y) {
		return (evtColision[x][y]) & (mapColision[x][y]);
	}
	
	void addZombie(int x,int y){
		ev.add("Zombie", "N", x, y, true, true);
		ev.events.get(ev.last).tags = "Zombi";
//		new jrig().AIGeneric(ev.last, 100, 200, new int[] { 1, 1 });
	}

	void SpawnAllZombies() {
			for (int iy = 1; iy < 50-1; iy++) {
				System.out.println("Zombie File: "+iy);
				for (int ix = 1; ix < 50-1; ix++) {
					if (Math.random()<0.1)
						addZombie(ix, iy);
				}
			}
			
	}
	
	
	void initMap() {
		try {
			BufferedImage icomand = ImageIO.read(new File("/home/ceibal/Imágenes/Maaaap/zombimapaa.png"));

			for (int iy = 1; iy < mapYsize-1; iy++) {
				System.out.println(iy);
				for (int ix = 1; ix < mapXsize-1; ix++) {
//					maplabel[ix][iy] = new JLabel("");
					mapColision[ix][iy] = true;
//					int icomandgetRGB = icomand.getRGB(ix, iy);
//					double thisrand = Math.random();
//					if (icomandgetRGB == new Color(175, 26, 25).getRGB())
//						maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Nuclear.png"));
//					else if (icomandgetRGB == new Color(153, 132, 69).getRGB()) {
//						if (thisrand < 0.01) {
//							maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Obstacles/TreeDry.png"));
//							mapColision[ix][iy] = false;
//						} else if (thisrand < 0.01 + 0.15)
//							maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Obstacles/DryGrassMark.png"));
//						else
//							maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Dry.png"));
//					} else if (icomandgetRGB == new Color(44, 91, 54).getRGB()) {
//						if (thisrand <= 0.16) {
//							maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Obstacles/TreeForest.png"));
//							mapColision[ix][iy] = false;
//						} else if (thisrand <= 0.16 + 0.09)
//							maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Obstacles/FlowerForest.png"));
//						else
//							maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Forest.png"));
//					} else if (icomandgetRGB == new Color(67, 140, 65).getRGB()) {
//						if (thisrand < 0.1) {
//							maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Obstacles/TreeForest.png"));
//							mapColision[ix][iy] = false;
//						} else if (thisrand < 0.17)
//							maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Obstacles/GrassMark.png"));
//						else if (thisrand < 0.25)
//							maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Obstacles/GrassPlant.png"));
//						else
//							maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Grass.png"));
//					} else if (icomandgetRGB == new Color(39, 115, 84).getRGB())
//						maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Taiga.png"));
//					else if (icomandgetRGB == new Color(255, 255, 255).getRGB())
//						maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Snow.png"));
//					else if (icomandgetRGB == new Color(65, 65, 65).getRGB())
//						maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Ash.png"));
//					else if (icomandgetRGB == new Color(90, 90, 64).getRGB())
//						maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Debris.png"));
//					else if (icomandgetRGB == new Color(250, 215, 150).getRGB())
//						maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Sand.png"));
//					else if (icomandgetRGB == new Color(255, 178, 127).getRGB())
//						maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Path.png"));
//					else if (icomandgetRGB == new Color(160, 160, 160).getRGB())
//						maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Rock.png"));
//					else if (icomandgetRGB == new Color(127, 51, 0).getRGB()) {
//						maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Logs.png"));
//						mapColision[ix][iy] = false;
//					} else if (icomandgetRGB == new Color(110, 140, 200).getRGB()) {
//						maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Water.png"));
//						mapColision[ix][iy] = false;
//					} else if (icomandgetRGB == new Color(180, 255, 0).getRGB()) {
//						maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Grass/Waste.png"));
//						mapColision[ix][iy] = false;
//					} else if (icomandgetRGB == new Color(127, 0, 0).getRGB()) {
//						maplabel[ix][iy].setIcon(new ImageIcon("/home/ceibal/Imágenes/Maaaap/Graph/Obstacles/Border.png"));
//						mapColision[ix][iy] = false;
//					}
//					mapPane.add(maplabel[ix][iy]);
//					maplabel[ix][iy].setBounds(ix * 32, iy * 32, 32, 32);
				}
			}
		} catch (Exception e) {
		}
	}

	void clearColishionEv() {
		for (int iy = 0; iy < mapYsize; iy++)
			for (int ix = 0; ix < mapXsize; ix++)
				evtColision[ix][iy] = true;
	}

}
