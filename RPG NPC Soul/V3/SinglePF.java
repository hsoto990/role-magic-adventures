package rma;

import java.awt.Point;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;

public class SinglePF {
	// GENERAL VARIABLES

	// private static int cells = 120;
	private int startx = 0;
	private int starty = 0;

	Point finpoint = new Point();
	// final static int MSIZE = 600;
	// BOOLEANS
	boolean solving = false;
	// UTIL
	Node[][] map;
	int catualEven = 0;

	public SinglePF() { // CONSTRUCTOR
		clearMap();
		for (int iy = 0; iy < dasdf.mapYsize; iy++)
			for (int ix = 0; ix < dasdf.mapXsize; ix++)
				if (!(dasdf.mapColision[ix][iy] & dasdf.evtColision[ix][iy]))
					map[ix][iy].cellType = 2;
	}

	public void clearMap() { // CLEAR MAP
		map = new Node[dasdf.mapXsize][dasdf.mapYsize]; // CREATE NEW MAP OF NODES
		for (int x = 0; x < dasdf.mapXsize; x++) {
			for (int y = 0; y < dasdf.mapYsize; y++) {
				map[x][y] = new Node(3, x, y); // SET ALL NODES TO EMPTY
			}
		}
		solving = false;
	}

	public void resetMap() { // RESET MAP
		for (int x = 0; x < dasdf.mapXsize; x++) {
			for (int y = 0; y < dasdf.mapYsize; y++) {
				Node current = map[x][y];
				// CHECK TO SEE IF CURRENT NODE IS EITHER CHECKED OR FINAL PATH
				if (current.cellType != 2)
					map[x][y] = new Node(3, x, y); // RESET IT TO AN EMPTY NODE
			}
		}

		startx = dasdf.ev.events.get(catualEven).pos.x;
		starty = dasdf.ev.events.get(catualEven).pos.y;
		map[startx][starty] = new Node(0, startx, starty);
		map[startx][starty].hops = 0;
		// if (finishx > -1 && finishy > -1)
		// map[finishx][finishy] = new Node(1, finishx, finishy);
		solving = false;
	}
	int time=0;
	public void resolve(int idevt, ArrayList<Point> finishes,int vtime) {
		time=vtime;
		catualEven = idevt;
		if (!finishes.contains(dasdf.ev.events.get(idevt).pos)) {

			new SinglePF();

			solving = false;

				if (finishes.size() > 0) { // IF NOT WALL
					resetMap(); // RESET THE MAP WHENEVER CLICKED
					for (int i = 0; i < finishes.size(); i++) {
//						if (map[finishes.get(i).x][finishes.get(i).y].cellType != 2)
							map[finishes.get(i).x][finishes.get(i).y].cellType = 1;
					}
					solving = true;
					new Algorithm().Dijkstra();
				}

			
		}
	}

	class Algorithm { // ALGORITHM CLASS

		public void Dijkstra() {
			ArrayList<Node> priority = new ArrayList<Node>(); // CREATE A PRIORITY QUE
			priority.add(map[startx][starty]); // ADD THE START TO THE QUE
			while (solving) {
				if (priority.size() <= 0) { // IF THE QUE IS 0 THEN NO PATH CAN BE FOUND
					solving = false;
					break;
				}
				int hops = priority.get(0).hops + 1; // INCREMENT THE HOPS VARIABLE
				ArrayList<Node> explored = exploreNeighbors(priority.get(0), hops); // CREATE AN ARRAYLIST OF
																								// NODES THAT
				// WERE EXPLORED
				if (explored.size() > 0) {
					priority.remove(0); // REMOVE THE NODE FROM THE QUE
					priority.addAll(explored); // ADD ALL THE NEW NODES TO THE QUE
				} else { // IF NO NODES WERE EXPLORED THEN JUST REMOVE THE NODE FROM THE QUE
					priority.remove(0);
				}
			}

		}

		public ArrayList<Node> exploreNeighbors(Node current, int hops) { // EXPLORE NEIGHBORS
			ArrayList<Node> explored = new ArrayList<Node>(); // LIST OF NODES THAT HAVE BEEN EXPLORED
			checkNeigh(current, hops, -1, 0, explored);
			checkNeigh(current, hops, +1, 0, explored);
			checkNeigh(current, hops, 0, -1, explored);
			checkNeigh(current, hops, 0, +1, explored);

			checkNeigh(current, hops, -1, -1, explored);
			checkNeigh(current, hops, +1, +1, explored);
			checkNeigh(current, hops, +1, -1, explored);
			checkNeigh(current, hops, -1, +1, explored);
			return explored;
		}

		void checkNeigh(Node current, int hops, int a, int b, ArrayList<Node> explored) {
			int xbound = current.x + a;
			int ybound = current.y + b;
			// MAKES SURE THE NODE IS NOT OUTSIDE THE GRID
			if ((xbound > -1) && (ybound > -1)) {
				Node neighbor = map[xbound][ybound];
				// CHECKS IF THE NODE IS NOT A WALL AND THAT IT HAS NOT BEEN EXPLORED
				if ((neighbor.hops == -1 || neighbor.hops > hops) && neighbor.cellType != 2) {
					explore(neighbor, current.x, current.y, hops); // EXPLORE THE NODE
					explored.add(neighbor); // ADD THE NODE TO THE LIST
				}
			}
		}

		public void explore(Node current, int lastx, int lasty, int hops) { // EXPLORE A NODE
			if (current.cellType != 0 && current.cellType != 1) // CHECK THAT THE NODE IS NOT THE START OR FINISH
				current.cellType = 4; // SET IT TO EXPLORED
			current.setLastNode(lastx, lasty); // KEEP TRACK OF THE NODE THAT THIS NODE IS EXPLORED FROM
			current.hops = hops; // SET THE HOPS FROM THE START
			if (current.cellType == 1) { // IF THE NODE IS THE FINISH THEN BACKTRACK TO GET THE PATH
				finpoint = new Point(current.x, current.y);
				backtrack(current.lastX, current.lastY, hops);
			}
		}

		public void backtrack(int lx, int ly, int hops) { // BACKTRACK
			ArrayList<Point> pathingu = new ArrayList<Point>();
			while (hops > 2) { // BACKTRACK FROM THE END OF THE PATH TO THE START
				Node current = map[lx][ly];
				current.cellType = 5;
				lx = current.lastX;
				ly = current.lastY;
				hops--;
			}
			pathingu.add(new Point(lx, ly));
			hops--;
			Point integPont = new Point();
			try {
				integPont=(new Point( pathingu.get(0).x - startx, pathingu.get(0).y - starty));
			} catch (Exception e) {

			}
			boolean mak = solving;
			resetMap();
			solving = mak;
				if (solving) {
					dasdf.ev.events.get(catualEven).Move(integPont.x, integPont.y, time);
				}
			
			solving = false;

		}
	}

	
	
	class Node {

		// 0 = start, 1 = finish, 2 = wall, 3 = empty, 4 = checked, 5 = finalpath
		private int cellType = 0;
		private int hops;
		private int x;
		private int y;
		private int lastX;
		private int lastY;

		public Node(int type, int x, int y) { // CONSTRUCTOR
			cellType = type;
			this.x = x;
			this.y = y;
			hops = -1;
		}

		public void setLastNode(int x, int y) {
			lastX = x;
			lastY = y;
		}

	}
}