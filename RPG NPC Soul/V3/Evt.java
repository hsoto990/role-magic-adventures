package rma;

import java.awt.Point;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Evt {
	int ID = -1;

	JLabel Eventlabel = new JLabel();
	String EventSkin = "";
	String EventPose = "";
	Point pos = new Point();
	String tags = "";
	Boolean lookwhenmoved = false;
	Boolean solid = true;
	Boolean standing = true;

	double damage = 0;

	public static Evt new_Evt(int x, int y, boolean lwm, boolean sol) {
		Evt tempEvt = new Evt();
		tempEvt.pos = new Point(x, y);
		tempEvt.lookwhenmoved = lwm;
		tempEvt.solid = sol;
		tempEvt.ID = dasdf.ev.last + 1;
		return tempEvt;
	}

	void setPoses(String pose) {
		EventPose = pose;
		Eventlabel.setIcon(new ImageIcon(dasdf.ev.SourceDir + "Graph/" + EventSkin + "/" + EventPose + ".png"));
	}

	void setChar(String chara, String pose) {
		EventSkin = chara;
		setPoses(pose);
	}

	void setPos(int index, int x, int y) {
		pos.setLocation(x, y);
		Eventlabel.setLocation(x * 32, y * 32);
	}

	void lookAtMock(int xmod, int ymod) {
		if (lookwhenmoved) {
			if (ymod == 1)
				setPoses("S");
			else if (ymod == -1)
				setPoses("N");
			else if (xmod == 1)
				setPoses("E");
			else if (xmod == -1)
				setPoses("W");
		}
	}

	void Move(int xmod, int ymod, int time) {
		if (standing) {
			standing = false;
			lookAtMock(xmod, ymod);
			if (solid) {
				if (dasdf.IsPassable(pos.x + xmod, pos.y + ymod)) {
					dasdf.evtColision[pos.x][pos.y] = true;
					dasdf.evtColision[pos.x + xmod][pos.y + ymod] = false;
					phantomMove(xmod, ymod, time);
				}
			} else
				phantomMove(xmod, ymod, time);
			standing = true;
		}
	}

	void unsolidify() {
		if (solid) {
			solid = false;
			dasdf.evtColision[pos.x][pos.y] = true;
		}
	}

	void phantomMove(int xmod, int ymod, int time) {
		pos.x += xmod;
		pos.y += ymod;
		for (int i = 0; i < 16; i++) {
			Eventlabel.setLocation(Eventlabel.getX() + xmod * 2, Eventlabel.getY() + ymod * 2);
			esperar(time);
		}
		Eventlabel.setLocation(pos.x * 32, pos.y * 32);
	}

	static void esperar(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}

}
