package rma;

import java.awt.Point;
import java.util.ArrayList;

public class AIBehavior {

	static int getDis(Point i1, Point i2) {
		return new GetDistance().resolve(i1, i2);
	}

	static void esperar(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}

	static ArrayList<Point> finisharround(int x, int y) {
		ArrayList<Point> finishes = new ArrayList<Point>();
		finishes.add(new Point(x, y));
		// finishes.add(new Point(x, y - 1));
		// finishes.add(new Point(x + 1, y - 1));
		// finishes.add(new Point(x - 1, y));
		// finishes.add(new Point(x + 1, y));
		// finishes.add(new Point(x - 1, y + 1));
		// finishes.add(new Point(x, y + 1));
		// finishes.add(new Point(x + 1, y + 1));
		return finishes;

	}

	static void AIZombie(Evt even) {
		new Thread() {
			public void run() {
				while (true) {
					if (even.standing) {
						int disToPlayer = getDis(even.pos, dasdf.ev.events.get(0).pos);
						if (disToPlayer == 1) {
							even.standing = false;
							String actmov = even.EventPose;
							even.setPoses("Atk");
							Evt.esperar(200);
							even.setPoses(actmov);
							esperar(50);
							even.standing = true;
						} else if (disToPlayer < 10) {

							new SinglePF().resolve(even.ID,
									finisharround(dasdf.ev.events.get(0).pos.x, dasdf.ev.events.get(0).pos.y));

						} else {
							int x = (int) (Math.random() * 4 - 2);
							int y = (int) (Math.random() * 4 - 2);
							dasdf.ev.events.get(even.ID).Move(x, y, 100);
						}
					}
				}
			}
		}.start();
	}
}
