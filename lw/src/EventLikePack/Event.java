package EventLikePack;

import java.awt.Color;
import java.util.ArrayList;

import Clases.*;
import ItemLikePack.Item;

public class Event {

	public Double px = .0;
	public Double py = .0;
	public Double fx = .0;
	public Double fy = .0;
	public Float ms = 0f;
	public Color cr;

	public int size = 20;

	public Angulo rotacion = new Angulo();

	public boolean pasable = false;
	public boolean fijo = false;

	public boolean attacking = false;
	public boolean building = false;

	public Item Equiped = null;
	public double baseDamage = 1;

	public double durability = 100;
	public double friction = 0.98;
	public ArrayList<String> varibles = new ArrayList<String>();
	public ArrayList<Item> Drops = new ArrayList<Item>();

	public Event(Double vpx, Double vpy, Double vfx, Double vfy, float vms, Color vcr) {
		px = vpx;
		py = vpy;
		fx = vfx * vms;
		fy = vfy * vms;
		ms = vms;
		cr = vcr;
	}
	
	public void beDamaged(double Force) {
		durability-= Math.pow(Math.abs(Force), 2)/100;
	}

}
