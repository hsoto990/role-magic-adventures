import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Tileset {
	ArrayList <Tile> tilesa = new ArrayList <Tile> ();
	Tileset() {
		tilesa.add(new Tile("Wall","Wall")); //Limite del mundo
		tilesa.add(new Tile("Grass","Ground"));//Pasto
		tilesa.add(new Tile("Sand","Ground"));//Arena
		tilesa.add(new Tile("Water1","Water"));//Agua
		tilesa.add(new Tile("Carpet","Red Carpet"));//Zona Segura
	}
	public BufferedImage getTileColor(int i) {
		return tilesa.get(i).getColor();
	}
}
