
public class Item {
	String name;
	double lenght = 0.6d;
	double VolumeUnis = 1d;
	double Sharpness = 1d;
	double thickness = 1d;
	double Trusting = 0d;
	double TrustWhenDamaged = 50d;

	double slashDamage = 0;
	double bluntDamage = 0;

	Material ThisMater = MaterialList.get("Carne");

	Item(String vname, double vlenght, double vBluntness, double vSharpness, double vHardness, double vDurability,
			double vTrusting, Material vMater) {
		name = vname;
		lenght = vlenght;
		VolumeUnis = vBluntness;
		Sharpness = vSharpness;
		// Durability = vDurability;
		Trusting = vTrusting;
		ThisMater = vMater;
	}

	void SwingAttack(Item Target) {
		double HardnessDiff = ThisMater.Hardness - Target.ThisMater.Hardness;
		// System.out.println("HardnessDiff: " + HardnessDiff);

		double SmashingForce = 5 * (0.5 + lenght) * VolumeUnis * ThisMater.Density;

		if (Trusting > 0) {
			double PiercingForce = SmashingForce * Trusting;
			// System.out.println("PiercingForce: " + PiercingForce);

			if (PiercingForce >= Target.ThisMater.neededToBeTrusted * Target.thickness) {
				// Target.Durability -= TrustWhenDamaged;
				double BluntForce = SmashingForce * (180 - Trusting) - Target.ThisMater.BluntAbsorb * Target.thickness;
				if (BluntForce < 0)
					BluntForce = 0;
				Target.bluntDamage += BluntForce / 531;
				// System.out.println("The attack pierced and caused " + TrustWhenDamaged + "PD
				// and " + BluntForce + "BD");

			} else {
				double BluntForce = SmashingForce * (180) - Target.ThisMater.BluntAbsorb * Target.thickness;
				if (BluntForce < 0)
					BluntForce = 0;
				Target.bluntDamage += BluntForce / 531;
				// System.out.println("The attack was unable to pierce and caused " + BluntForce
				// + "BD");
			}

		} else if (HardnessDiff > 0) {
			double CuttingForce = SmashingForce * Math.pow(Sharpness, 10) / Math.pow(180, 9);
			double BluntForce = SmashingForce * (180 - Sharpness) - Target.ThisMater.BluntAbsorb * Target.thickness;
			if (BluntForce < 0)
				BluntForce = 0;

			// if (Sharpness > 0)
			// System.out.println(
			// "The attack slashed the target and caused " + CuttingForce + "SD and " +
			// BluntForce + "BD");
			// else
			// System.out.println("The attack bashed the target and caused " + BluntForce +
			// "BD");
			// Target.Durability -= CuttingForce;
			Target.slashDamage += (CuttingForce / (23 * 2.5)) / Target.thickness * HardnessDiff;
			Target.bluntDamage += BluntForce / 531;
		} else {
			double BluntForce = SmashingForce * (180) - Target.ThisMater.BluntAbsorb * Target.thickness;
			if (BluntForce < 0)
				BluntForce = 0;
			Target.bluntDamage += BluntForce / 531;
			// System.out.println("The attack bashed the target and caused " + BluntForce +
			// "BD");
		}

		System.out.println("----------------------------");

		System.out.print(Target.slashDamage + " - ");

		if (Target.slashDamage == 0)
			System.out.println("No tiene ni un rasguño");
		else if (Target.slashDamage <= 10 * thickness)
			System.out.println("Tis' but a scratch");
		else if (Target.slashDamage <= 20 * thickness)
			System.out.println("Corte superficial");
		else if (Target.slashDamage <= 40 * thickness)
			System.out.println("Corte leve");
		else if (Target.slashDamage <= 50 * thickness)
			System.out.println("Corte Moderado");
		else if (Target.slashDamage <= 60 * thickness)
			System.out.println("Corte Profundo");
		else if (Target.slashDamage <= 80 * thickness)
			System.out.println("Corte muuy Profundo");
		else if (Target.slashDamage < 100 * thickness)
			System.out.println("Corte mega Profundo");
		else
			System.out.println("Amputación");

		System.out.print(Target.bluntDamage + " - ");

		if (Target.bluntDamage == 0)
			System.out.println("No tiene contusiones");
		else if (Target.bluntDamage <= 15 * thickness)
			System.out.println("Contusiones mínimas");
		else if (Target.bluntDamage <= 35 * thickness)
			System.out.println("Contusiones leves");
		else if (Target.bluntDamage <= 60 * thickness)
			System.out.println("Contusiones moderadas");
		else if (Target.bluntDamage < 100 * thickness)
			System.out.println("Contusiones graves");
		else
			System.out.println("Todo hecho puré");

	}

}
