import java.util.ArrayList;

public class ExtraVars {

	
	ArrayList<String> Name = new ArrayList<String>();
	ArrayList<Integer> Value = new ArrayList<Integer>();
	
	void add(String vName, int vValue){
		Name.add(vName);
		Value.add(vValue);
	}
	Integer getVal(String vName) {
		return Value.get(Name.indexOf(vName));
	}
	void setVal(String vName,int seta) {
		Value.set(Name.indexOf(vName),seta);
	}
	void AltVal(String vName,int mod) {
		Value.set(Name.indexOf(vName),getVal(vName)+mod);
	}
}
