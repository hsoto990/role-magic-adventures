public class EventPrefabs {
	public static Event tree(int x, int y) {
		Event preb = new Event(x, y, "Oak");
		preb.mode = "TreeGrowing";
		preb.EV.add("Maturity", 0);
		preb.EV.add("State", 1);
		preb.setImage("bTree/5");
		preb.itemOfEvent = new Item("Tree", 2,0.5,0,0,100,0,MaterialList.get("Madera"));
		preb.itemOfEvent.thickness=15;
		return preb;
	}

	public static Event rock(int x, int y) {
		Event preb = new Event(x, y, "Rock");
		preb.setImage("Rocka");
		preb.itemOfEvent = new Item("Rock", 0.1, 5, 0, 2, 100, 0, MaterialList.get("Ortoclasa"));
		return preb;
	}

	public static Event human(int x, int y) {
		Event preb = new Event(x, y, "Human");
		preb.WalkOn.add("Ground");
		preb.WalkOn.add("Red Carpet");
		preb.WalkOn.add("Water");
		preb.speed = 4;
		preb.itemOfEvent = new Item("Flesh", 0.1, 1, 0, 0, 100, 0, MaterialList.get("Carne"));
		return preb;
	}

	public static Event fish(int x, int y) {
		Event preb = new Event(x, y, "Fish");
		preb.WalkOn.add("Water");
		preb.mode = "Roam";
		preb.itemOfEvent = new Item("Fish", 0.1, 1, 0, 0, 100, 0, MaterialList.get("Carne"));
		return preb;
	}
}
