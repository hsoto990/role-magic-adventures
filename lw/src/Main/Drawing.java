package Main;

import java.awt.Color;
import java.awt.Graphics;
import Clases.Angulo;
import EventLikePack.Event;

public class Drawing {

	public static void Draw(Graphics g) {
		try {
			g.setColor(new Color(150,200,80));
			g.fillRect(0, 0, 530, 530);
		for (Event i : Main.Events) {
			Event ja = (Event) i;
			g.setColor(ja.cr);
			int isi= i.size;
			g.fillOval((int) (ja.px * 1) - isi/2, (int) (ja.py * 1) - isi/2, isi, isi);

			g.setColor(Color.RED);
			for (int ix = 0; ix < 53; ix++) {
				for (int iy = 0; iy < 53; iy++) {
					if (!Timerendo.DamageGrid.get(ix).get(iy).isEmpty())
						g.fillRect(ix * 10, iy * 10, 10, 10);
				}
			}

			Angulo mija = ja.rotacion;
			g.setColor(new Color(0,0,0,100));

			g.fillOval((int) ((ja.px * 1) + mija.getX()*ja.size/2) -3, (int) ((ja.py * 1) + mija.getY()*ja.size/2)-3, 6, 6);
			
			
		}
			g.drawImage(Main.Events.get(0).Equiped.Icono.imageVar, 10, 450, 50, 50, null);
			g.drawString(Main.Events.get(0).Equiped.Nombre.TextoVar, 70, 480);
			g.drawString("HP: "+Main.Events.get(0).durability, 20, 20);
		}catch(Exception e) {
			
		}
	}

}
