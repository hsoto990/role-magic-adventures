package Main;

import java.awt.Color;
import java.util.ArrayList;
import EventLikePack.DoEveryTick;
import EventLikePack.Event;

//import Clases.*;

public class Timerendo {
	public static int actualEvent = -1;

	public static ArrayList<ArrayList<ArrayList<Integer>>> DamageGrid = new ArrayList<ArrayList<ArrayList<Integer>>>();

	static void Run() {
		for (int i = 0; i < 53; i++) {
			DamageGrid.add(new ArrayList<ArrayList<Integer>>());
			for (int i2 = 0; i2 < 53; i2++) {
				DamageGrid.get(i).add(new ArrayList<Integer>());
			}
		}
		new Thread() {
			public void run() {
				while (true) {

					for (int i = 0; i < 53; i++)
						for (int i2 = 0; i2 < 53; i2++)
							DamageGrid.get(i).get(i2).clear();

					int BanMod = 0;
					for (int i = 0; i < Main.BanList.size(); i++) {
						Main.Events.remove(Main.BanList.get(i).intValue() - BanMod);
						BanMod++;
						
						while (Fisica.Fijacion.ln1.indexOf(Main.BanList.get(i)) !=-1) {
							int jima =Fisica.Fijacion.ln1.indexOf(Main.BanList.get(i));
							Fisica.Fijacion.ln1.remove(jima);
							Fisica.Fijacion.ln2.remove(jima);
						}while (Fisica.Fijacion.ln2.indexOf(Main.BanList.get(i)) !=-1) {
							int jima =Fisica.Fijacion.ln2.indexOf(Main.BanList.get(i));
							Fisica.Fijacion.ln1.remove(jima);
							Fisica.Fijacion.ln2.remove(jima);
						}
					
						Fisica.Fijacion.jamod++;
					}
					Main.BanList.clear();

					Controles.ForTimerendo();
					
					// REPULSION
					Fisica.Repulsion.compute();
					//FIJACION
					Fisica.Fijacion.compute();

					Fisica.Movimiento.compute();
					for (int i = 0; i < Main.Events.size(); i++) {
						Event tempPash = Main.Events.get(i);

						double pikuxa = tempPash.rotacion.getX();
						double pikuya = tempPash.rotacion.getY();
						try {

							if (Main.Events.get(i).Equiped.RestantSwTime > 0)
								Main.Events.get(i).Equiped.RestantSwTime--;
							else {
								if (Main.Events.get(i).building) {
									Main.Events.get(i).building = false;
									try {
										Event temEvent = new Event(tempPash.px + pikuxa * 16, tempPash.py + pikuya * 16,
												pikuxa * 50, pikuya * 50, 1f, new Color(255, 0, 0));
										// temEvent.fijo = true;
										temEvent.size = 5;
										temEvent.durability = 1;
										temEvent.friction = 1;
										// temEvent.varibles.add("Vanishing");
										Main.Events.add(temEvent);
										Main.Events.get(i).Equiped.RestantSwTime = Main.Events.get(i).Equiped.SwingTime;
									} catch (Exception e) {
										break;
									}
								}
							}
						} catch (Exception e) {
						}
					}
					Main.frame.repaint();

					Esperar(10);
				}
			}
		}.start();

	}

	public static void Esperar(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}
}
