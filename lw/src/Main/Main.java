package Main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.*;

import Clases.Numero;
import Clases.Texto;
import EventLikePack.Event;
import ItemLikePack.Item;

import java.awt.event.KeyAdapter;

public class Main extends JPanel {
	private static final long serialVersionUID = 1L;

	public static ArrayList<Event> Events = new ArrayList<Event>();
	public static ArrayList<Integer> BanList = new ArrayList<Integer>();

	public Main() {
		try {
			// frame.setUndecorated(true);

		} catch (Exception e) {

		}
		// NOTHINGNESS
	}

	@Override
	protected void paintComponent(Graphics g) {
		Drawing.Draw(g);
	}

	public static JFrame frame = new JFrame("Lisu");

	// create the GUI explicitly on the Swing event thread
	private static void createAndShowGui() {

		Main mainPanel = new Main();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(mainPanel);
		frame.setVisible(true);
		frame.setBounds(240, 0, 530, 530);

		Events.add(new Event(40d, 40d, 0d, 0d, 1f, Color.BLACK));
		Events.get(Events.size() - 1).Equiped = new Item("Gun", "Ak47", "Rifle de asalto", 100, 5);

		for (int i = 0; i < 50; i++) {
			Event tempEvent = new Event(20d + i * 20, 230d, 0d, 0d, 1f, Color.ORANGE);
			tempEvent.friction = 0.90;
			Events.add(tempEvent);

		}

		Events.add(new Event(370d, 230d, 0d, 0d, 1f, new Color(0, 100, 0)));
		Events.get(Events.size() - 1).fijo = true;
		Events.get(Events.size() - 1).size = 40;

		Events.add(new Event(270d, 230d, 0d, 0d, 1f, new Color(100, 200, 100)));
		// Events.get(Events.size()-1).pasable=true;
		Events.get(Events.size() - 1).fijo = true;
		Events.get(Events.size() - 1).size = 10;

		Events.add(new Event(220d, 230d, 0d, 0d, 1f, new Color(100, 200, 100)));
		Events.get(Events.size() - 1).pasable = true;
		Events.get(Events.size() - 1).fijo = true;
		Events.get(Events.size() - 1).size = 15;
		// tempEv=new Event();
		// tempEv.px=5d;
		// tempEv.py=2d;
		//
		// Events.add(tempEv);
		
		Fisica.Fijacion.makeSquare(100, 100, 4, 4, Color.CYAN, 10, 0.1f, 0, 0);

		Timerendo.Run();
		frame.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				Controles.playerPressOrRelease(true, arg0);
			}

			public void keyReleased(KeyEvent arg0) {
				Controles.playerPressOrRelease(false, arg0);
			}
		});

		new Main();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGui();
			}
		});
	}
}