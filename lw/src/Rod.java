import java.awt.Color;

public class Rod {
	double thickness = 0;
	double height = 0;
	
	double sharpness=0;
	double bluntness=0;

	double thicknessGrowness = 0;
	double heightGrowness = 0;

	int leftToMature = 1;

	Color co = new Color(53, 255, 5);

	double dacR = co.getRed();
	double dacG = co.getGreen();
	double dacB = co.getBlue();

	Color coMatt = new Color(137, 88, 25);

	
	double difR = (co.getRed()-coMatt.getRed())/leftToMature;
	double difG = (co.getGreen()-coMatt.getGreen())/leftToMature;
	double difB = (co.getBlue()-coMatt.getBlue())/leftToMature;
	
	void newsaaa(double vthickness,double vheight,double vthicknessGrowness, double vheightGrowness,Color c1, Color c2, int ltm) {
		thickness=vthickness;
		height=vheight;
		thicknessGrowness=vthicknessGrowness;
		heightGrowness=vheightGrowness;
		setColor(ltm,c1,c2);
	}
	
	void grow() {
		if (height < 64) {
			thickness += thicknessGrowness;
			height += heightGrowness;
		}
		if (leftToMature > 0) {
			leftToMature--;
			dacR += difR;
			dacG += difG;
			dacB += difB;
			co = new Color((int) dacR, (int) dacG, (int) dacB);
		}
	}

	void growness(double tg, double hg) {
		thicknessGrowness = tg;
		heightGrowness = hg;
	}
	
	void setColor(int ltm, Color inma, Color matu) {
		dacR = inma.getRed();
		dacG = inma.getGreen();
		dacB = inma.getBlue();
		co = inma;
		coMatt=matu;
		leftToMature=ltm;
		difR = ((double)(-co.getRed()+coMatt.getRed()))/leftToMature;
		difG = ((double)(-co.getGreen()+coMatt.getGreen()))/leftToMature;
		difB = ((double)(-co.getBlue()+coMatt.getBlue()))/leftToMature;
	}
	
}
