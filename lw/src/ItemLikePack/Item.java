package ItemLikePack;

import Clases.*;

public class Item {

	Texto Categoria = new Texto("Untitled");

	public Imagen Icono = IconImage(new Texto(""));
	public Texto Nombre = new Texto("?");
	public Numero Durabilidad = new Numero(100);
//	public double MiningForce = 0;
//	public double ChoppingForce = 0;
//	public double DiggingForce = 0;
//	public double EntityDamage = 1;
	public int SwingTime = 1;
	public int RestantSwTime = 0;
	
	public Item (String vCat,String vIcon, String vNom, int vDur,int vSped) {
		Categoria = new Texto(vCat);
		Icono = IconImage(new Texto(vIcon));
		Nombre = new Texto(vNom);
		Durabilidad = new Numero(vDur);			
		SwingTime = vSped;
	}
	

	Imagen IconImage(Texto vDir) {
		return new Imagen(Categoria.TextoVar + "/" + vDir.TextoVar);
	}

}
