package ItemLikePack;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class Inventory {
	ArrayList<Item> inven = new ArrayList<Item>();

	Item Equiped = null;
	int selItem = 0;

	void add(Item mikusi) {
		inven.add(mikusi);
	}

	void show(Graphics g) {
		g.setColor(new Color(180, 210, 210));
		g.fillRect(0, 0, 530, 530);
		g.setColor(new Color(200, 255, 255));
		g.fillRect(50 - 8, 180 - 14, 400, 18);
		g.setColor(Color.black);
		g.drawString(Equiped.Nombre.TextoVar, 50, 30);
		for (int i = selItem - 5; i < selItem + 5; i++) {
			if (i >= 0 & i < inven.size())
				g.drawString(inven.get(i).Nombre.TextoVar, 50, 180 + 20 * (i - selItem));
		}
	}

	void equip() {
		if (inven.size() > 0) {
			Item tempSaaaa = Equiped;
			Equiped = inven.get(selItem);
			inven.remove(selItem);
			if (!tempSaaaa.Nombre.TextoVar.equals("-"))
				inven.add(tempSaaaa);
		}
	}

	void unequip() {
		if (!Equiped.Nombre.TextoVar.equals("-")) {
			inven.add(Equiped);
			Equiped = null;
		}
	}

}
