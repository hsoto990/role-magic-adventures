import java.awt.image.BufferedImage;

public class TileLayer {
	int[][] tila;
	Tileset tise=new Tileset();
	TileLayer(int xs, int ys){
		tila= new int[xs][ys];
	}
	public void setTile(int ix,int iy, int titi) {
		tila[ix][iy]=titi;
	}
	public BufferedImage getTileColor(int ix,int iy) {
		return tise.tilesa.get(tila[ix][iy]).getColor();
	}
}
