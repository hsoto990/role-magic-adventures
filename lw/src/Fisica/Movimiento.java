package Fisica;

import EventLikePack.DoEveryTick;
import Main.Main;
import Main.Timerendo;

public class Movimiento {

	public static void compute() {
		for (Timerendo.actualEvent = 0; Timerendo.actualEvent < Main.Events.size(); Timerendo.actualEvent++) {
			int i = Timerendo.actualEvent;

			if (Main.Events.get(i).fijo) {
				Main.Events.get(i).fx = 0d;
				Main.Events.get(i).fy = 0d;
			} else {

				float mas = Main.Events.get(i).ms;

				Main.Events.get(i).px += Main.Events.get(i).fx / mas / 10 + (Math.random() - 0.5) / 4;
				Main.Events.get(i).py += Main.Events.get(i).fy / mas / 10 + (Math.random() - 0.5) / 4;
				Main.Events.get(i).fx *= Main.Events.get(i).friction;
				Main.Events.get(i).fy *= Main.Events.get(i).friction;

				Limite(i, 0,0,530,530);
			}
			DoEveryTick.StartPoint(Main.Events.get(i));
		}
	}

	static void Limite(int i, int minX, int minY, int maxX, int maxY) {
		if (Main.Events.get(i).py > maxY) {
			Main.Events.get(i).py = maxY*1d;
			if (Main.Events.get(i).fy > 0) {
				Main.Events.get(i).fy = -Main.Events.get(i).fy;
				Main.Events.get(i).beDamaged(Main.Events.get(i).fy);
			}
		} else if (Main.Events.get(i).py < minY) {
			Main.Events.get(i).py = minY*1d;
			if (Main.Events.get(i).fy < 0) {
				Main.Events.get(i).fy = -Main.Events.get(i).fy;
				Main.Events.get(i).beDamaged(Main.Events.get(i).fy);
			}
		}
		if (Main.Events.get(i).px > maxX) {
			Main.Events.get(i).px = maxX*1d;
			if (Main.Events.get(i).fx > 0) {
				Main.Events.get(i).fx = -Main.Events.get(i).fx;
				Main.Events.get(i).beDamaged(Main.Events.get(i).fx);
			}
		} else if (Main.Events.get(i).px < minX) {
			Main.Events.get(i).px = minX*1d;
			if (Main.Events.get(i).fx < 0){
				Main.Events.get(i).fx = -Main.Events.get(i).fx;
				Main.Events.get(i).beDamaged(Main.Events.get(i).fx);
			}
		}
	}
}
