package Fisica;

import java.awt.Color;
import java.util.ArrayList;

import EventLikePack.Event;
import Main.Main;

public class Fijacion {

	public static ArrayList<Integer> ln1 = new ArrayList<Integer>();
	public static ArrayList<Integer> ln2 = new ArrayList<Integer>();
	public static int jamod = 0;

	public static void compute() {
		int o = 0;
		while (o < ln1.size()) {
			int i1 = ln1.get(o) - jamod;
			int i2 = ln2.get(o) - jamod;
			System.out.println(jamod + " " + i1 + " " + i2);
			try {
				double disx = Main.Events.get(i1).px - Main.Events.get(i2).px;
				double disy = Main.Events.get(i1).py - Main.Events.get(i2).py;
				double dis = disx * disx + disy * disy;

				dis = Math.sqrt(dis);
				double Force = (dis) * (Main.Events.get(i1).ms + Main.Events.get(i2).ms);
				double XForce = Force * disx / dis;
				double YForce = Force * disy / dis;

				Main.Events.get(i1).fx -= XForce;
				Main.Events.get(i1).fy -= YForce;
				Main.Events.get(i2).fx += XForce;
				Main.Events.get(i2).fy += YForce;
			} catch (Exception e) {

			}
			o++;

		}
	}

	public static void makeSquare(double x, double y, int anc, int alt, Color saaa, int sep, float mas, double fx,
			double fy) {
		for (int iy = 0; iy < alt; iy++) {
			for (int ix = 0; ix < anc; ix++) {
				Event teja = new Event(x + ix * sep, y + iy * sep, fx, fy, mas, saaa);
				teja.size = (int) (sep * 2);
				System.out.println("aaa");
				Main.Events.add(teja);
				if (ix != 0) {
					ln1.add(Main.Events.size() - 2);
					ln2.add(Main.Events.size() - 1);
				}
				if (iy != 0) {
					ln1.add(Main.Events.size() - anc - 1);
					ln2.add(Main.Events.size() - 1);

				}
			}

		}
	}
}
