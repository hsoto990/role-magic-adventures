package Fisica;

import Main.Main;

public class Repulsion {

	public static void compute() {
		for (int i = 0; i < Main.Events.size() - 1; i++) {
			double xp = Main.Events.get(i).px;
			double yp = Main.Events.get(i).py;
			for (int sf = 0; sf < Main.Events.size(); sf++) {
				if (!(Main.Events.get(i).pasable | Main.Events.get(sf).pasable)) {

					double disx = xp - Main.Events.get(sf).px;
					double disy = yp - Main.Events.get(sf).py;
					double dis = disx * disx + disy * disy;
					int dosice = (Main.Events.get(i).size + Main.Events.get(sf).size) / 2;
					if (dis < dosice * dosice & dis != 0) {
						dis = Math.sqrt(dis);
						double Force = (dosice - dis) * (Main.Events.get(i).ms + Main.Events.get(sf).ms);
						double XForce = Force * disx / dis;
						double YForce = Force * disy / dis;

						Main.Events.get(i).fx += XForce;
						Main.Events.get(i).fy += YForce;
						Main.Events.get(sf).fx -= XForce;
						Main.Events.get(sf).fy -= YForce;
						double promx = (Main.Events.get(sf).fx + Main.Events.get(i).fx) / 2;
						double promy = (Main.Events.get(sf).fy + Main.Events.get(i).fy) / 2;

						Main.Events.get(i).fx = (promx + Main.Events.get(i).fx * 100) / 101;
						Main.Events.get(i).fy = (promy + Main.Events.get(i).fy * 100) / 101;
						Main.Events.get(sf).fx = (promx + Main.Events.get(sf).fx * 100) / 101;
						Main.Events.get(sf).fy = (promy + Main.Events.get(sf).fy * 100) / 101;

						Main.Events.get(i).beDamaged(Force);
						Main.Events.get(sf).beDamaged(Force);
					}
				}
			}

		}
	}
	
}
