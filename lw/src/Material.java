
public class Material {
	String Name = "";
	double Hardness = 1d;
	double Density = 1;
	double neededToBeTrusted = 0d;
	double neededToBeCracked = 0d;
	double BluntAbsorb = 0;

	Material (String vName, double vHardness,double vDensity,double vneededToBeTrusted, double vneededToBeCracked){
		Name=vName;
		Hardness = vHardness;
		Density = vDensity;
		neededToBeTrusted = vneededToBeTrusted;
		neededToBeCracked = vneededToBeCracked;
	}
}
