package Clases;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Imagen {
	public BufferedImage imageVar = null;

	public Imagen(String vDirIma) {
		try {
			imageVar = ImageIO.read(new File("/home/ceibal/res/" + vDirIma + ".png"));
		} catch (IOException e) {
			System.out.println("Textura '" + vDirIma + "' no encontrada");
		}
	}
}
