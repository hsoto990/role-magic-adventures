package Clases;

import java.util.ArrayList;

public class Array {

	public ArrayList <Object> arrayVar = new ArrayList <Object> ();
	
	public void add(Object varArray) {
		arrayVar.add(varArray);
	}
	
	public Object get(Numero index) {
		return arrayVar.get((int)index.NumeroVar);
	}
	
	public Numero indexOf (Object varObject) {
		return new Numero (arrayVar.indexOf(varObject));
	}
	
	public Numero last () {
		return new Numero(arrayVar.size()-1);
		
	}
}
