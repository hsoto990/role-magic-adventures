package Clases;

import java.awt.Point;

public class Angulo {
	public double AnguloVar = 180;

	public void alter(double ima) {
		AnguloVar += ima;
		if (AnguloVar >= 360) {
			alter(-360);
		} else if (AnguloVar < 0) {
			alter(360);
		}
	}

	public double getX() {
		return Math.cos(AnguloVar);
	}
	public double getY() {
		return Math.sin(AnguloVar);
	}

	
}
